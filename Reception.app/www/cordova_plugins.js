cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "id": "mobi.monaca.plugins.Monaca.monaca",
        "file": "plugins/mobi.monaca.plugins.Monaca/www/monaca.js",
        "pluginId": "mobi.monaca.plugins.Monaca"
    },
    {
        "id": "mobi.monaca.plugins.WebIntent.Webintent",
        "file": "plugins/mobi.monaca.plugins.WebIntent/www/webintent.js",
        "pluginId": "mobi.monaca.plugins.WebIntent",
        "clobbers": [
            "plugins.webintent"
        ]
    },
    {
        "id": "cordova-plugin-splashscreen.SplashScreen",
        "file": "plugins/cordova-plugin-splashscreen/www/splashscreen.js",
        "pluginId": "cordova-plugin-splashscreen",
        "clobbers": [
            "navigator.splashscreen"
        ]
    },
    {
        "id": "cordova-plugin-printer.Printer",
        "file": "plugins/cordova-plugin-printer/www/printer.js",
        "pluginId": "cordova-plugin-printer",
        "clobbers": [
            "plugin.printer",
            "cordova.plugins.printer"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "mobi.monaca.plugins.Monaca": "3.1.0",
    "mobi.monaca.plugins.WebIntent": "1.0.0",
    "cordova-custom-config": "2.0.3",
    "cordova-plugin-whitelist": "1.3.1",
    "cordova-plugin-splashscreen": "4.0.3",
    "cordova-plugin-crosswalk-webview": "2.3.0",
    "cordova-plugin-printer": "0.7.4-dev"
};
// BOTTOM OF METADATA
});