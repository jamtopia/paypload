
/* debug section to allow you to turn on debuging and point to a local url */


var initialHref = document.location.pathname;


var usernamedebug = ""; 
var passworddebug = "";

var savedpassword = "";
var savedusername = "";
var savedprinter = "";

/* main scren variables */
var hasStaff = false;

/* plugins loaded */
var pluginsLoaded = false;


/* Printer ready */
var printLabels = false;
var hasPrinter = false;
var printerURL = '';

/* global timer variables */
var promise;

/* online flag */

var isOnline = true;

/* store called back data from server */
var prevnames;
var vistors;
var prefilledvisitors;
var company;
var categories;
var departments;
var showcategoriespage = false;


/* checkin vars */
var checkinactive = false;
var patient;
var person_visiting;
var checkin_firstname;
var checkin_reg;
var checkin_surname;
var checkin_email;
var checkin_mobile;
var checkin_badgeno;
var checkin_company;

var dbs_number;
var dbs_expire;


var get_here;
var optin;
var rating; 
var checkin_categoryId = 0;
var checkin_categoryName = '';
var checkin_ndaEnabled = 'N';
var checkin_categoryVistorRequest = true;
var checkin_signature = '';
var checkin_signature_accepted = 'N';
var checkin_category;


var company_override_message = '';


/* checkout vars */
var rating = '';
var postcode = '';
var distance = '';       


/* offline control variables */
var checkincache = [];
var checkinactive = false;
var checkoutcache = [];
var checkoutactive = false;

var lostbadgecache = [];

var screenredirectdelay = 40000;
var thanksredirectdelay = 5000;


var debug = false;

// For IOS and hide he registration button
var showRegister = false;


var version = "3.0.35-build0226";
if (debug) { version = version + 'debug'; }       

var baseurl = "https://reception.mediabasedirect.com";


/*
Override the standard javascript logging and keep for later use
*/

var logArray = {
      logs: [],
      errors: [],
      warns: [],
      infos: []
    };      

if (!debug) {

    (function(){
      var _log = console.log;
      var _error = console.error;
      var _warning = console.warning;



      console.error = function(errMessage){
          logArray.errors.push(arguments);
         _error.apply(console,arguments);
      };

      console.log = function(logMessage){
          // Do something with the log message
          logArray.logs.push(arguments);
          _log.apply(console,arguments);
      };

      console.warning = function(warnMessage){
         // do something with the warn message
          logArray.warns.push(arguments);
         _warning.apply(console,arguments);
      };
      
    })();

}

/*

Print checkin label

checkin_firstname

*/
function printLabel() {



    if (hasPrinter) {

        var checkinTime = new Date(); 
        var labelToPrint = user.label;

        labelToPrint = labelToPrint.replace('data:visitorfname', checkin_firstname);
        labelToPrint = labelToPrint.replace('data:visitorname', checkin_firstname + ' ' + checkin_surname);
        labelToPrint = labelToPrint.replace('data:visitingname', patient);
        labelToPrint = labelToPrint.replace('data:terminalname', user.terminal_name);
        labelToPrint = labelToPrint.replace('data:visitorcheckintime', moment(checkinTime).format("YYYY-MM-DD HH:mm:ss"));
        labelToPrint = labelToPrint.replace('data:visitorcategory', checkin_categoryName);

        cordova.plugins.printer.print('<html><body>' + labelToPrint + '</body></html>', { landscape:true , printerId: printerURL, hidePageRange: true, hideNumberOfCopies: true, hidePaperFormat: true }, function (res) {
        });  

    }    


}

function saveLostBadge(name,badge,manager,managerid,department,departmentid) {

    var checkinTime = new Date(); 

    lostbadgecache.push({name: name
                                , badge: badge
                                , manager: manager
                                , managerid: managerid
                                , department: department
                                , departmentid: departmentid
                                , checkin_time : moment(checkinTime).format("YYYY-MM-DD HH:mm:ss")
                                });

    $('#spinner').hide();    
    myNavigator.replacePage('thankslost.html', { animation : 'none' });

    if (debug ) { console.log(lostbadgecache); }

}



/* Do prefilled/already registerd checkin */

function doPrefilledCheckin($http,prefillcheckinId){
    
    $('#spinner').show(); 

    $http({
                method  : 'POST',
                url     : baseurl + "/api/visitor/prefilledcheckin/" + prefillcheckinId ,
                headers: {
                    "X-Auth-Token": user.access_token,   //If your header name has spaces or any other char not appropriate
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                //data    : $.param({checkout_time: checkoutdata.choutout_time, rating: checkoutdata.rating, postcode: checkoutdata.postcode, distance: checkoutdata.distance}),  // pass in data as strings            
                dataType: 'json'
            }).success(function(data) {
                prefillcheckindata = data.results;
                person_visiting = prefillcheckindata.person_visiting;
                checkinTime = new Date(); 
                checkin_firstname = prefillcheckindata.first_name;
                checkin_surname = prefillcheckindata.last_name;
                checkin_categoryName = prefillcheckindata.categoryName;

                printLabel();
                $('#spinner').hide(); 
                myNavigator.replacePage('thanks.html',{ animation : 'none' });

            }).error(function (error, status){
                console.error({ message: error, status: status});
                isOnline = false;         

                $('#spinner').hide(); 
            });




}


/*

Saves checking for later send

*/


function saveCheckin() {

    var checkinTime = new Date(); 

    checkincache.push({first_name: checkin_firstname
                                , car_reg: checkin_reg
                                , last_name: checkin_surname
                                , company_representing: patient
                                , email: checkin_email
                                , company_id: patient_id
                                , title : checkin_mobile
                                , travel_type : get_here
                                , optin : optin
                                , checkin_company : checkin_company
                                , checkin_time : moment(checkinTime).format("YYYY-MM-DD HH:mm:ss")
                                , checkin_categoryId : checkin_categoryId
                                , checkin_categoryName : checkin_categoryName
                                , checkin_badgeno : checkin_badgeno
                                , checkin_signature_accepted : checkin_signature_accepted
                                , checkin_signature : checkin_signature

                                });


    printLabel();

    $('#spinner').hide();    
    myNavigator.replacePage('thanks.html', { animation : 'none' });

    if (debug ) { console.log(checkincache); }

    if (checkin_email === '') {
        lastemailblank = true;
    }
    else
    {
        lastemailblank = false;
    }

}

/* 

Save chouk out for later send

*/

// This is a JavaScript file

function saveCheckout(checkoutid,redirect) {
    
    if (redirect === undefined) {
        redirect = true;
    }

    var checkoutTime = new Date(); 

    checkoutcache.push({checkoutid: checkoutid, choutout_time: moment(checkoutTime).format("YYYY-MM-DD HH:mm:ss"), rating: rating, postcode: postcode, distance: distance});

    if (redirect) {
        myNavigator.replacePage('thankscheckout.html', { animation : 'none' });
    }

    if (debug ) { console.log(checkoutcache); }

}



/*

Loops the array with checkouts

*/

function processCachedOut($http,$scope){

    // Loop through any saved checkins
    for (var i = 0; i < checkoutcache.length; i++) {

        sendcheckoutOffLine($http,$scope,checkoutcache[i],i);
    }

}

/* 

Loops though array with checkins

*/


function processCached($http,$scope){

    // Loop through any saved checkins
    for (var i = 0; i < checkincache.length; i++) {

        sendcheckinOffLine($http,$scope,checkincache[i],i);
    }



    // Loop through any saved checkins
    for (var i = 0; i < lostbadgecache.length; i++) {

        sendlostBadgeOffLine($http,$scope,lostbadgecache[i],i);
    }

}


/* 

Send the checkout's that are passed to it

*/


function sendcheckoutOffLine($http,$scope,checkoutdata,arrayid) {

        if ( checkoutdata.checkoutid > 0 ) {

            $http({
                method  : 'POST',
                url     : baseurl + "/api/visitor/checkout/" + checkoutdata.checkoutid ,
                headers: {
                    "X-Auth-Token": user.access_token,   //If your header name has spaces or any other char not appropriate
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data    : $.param({checkout_time: checkoutdata.choutout_time, rating: checkoutdata.rating, postcode: checkoutdata.postcode, distance: checkoutdata.distance}),  // pass in data as strings            
                dataType: 'json'
            }).success(function(data) {
                checkout = data.results;
              
                if (debug ) { console.log(company); }
                checkoutcache.splice(arrayid, 1);
                
            }).error(function (error, status){

                console.error({ message: error, status: status});
                isOnline = false;         
        
            });    
        }
        else
        {
            console.log(checkoutdata);
        }
}

/*

Send checking ins that are passed

*/


function sendcheckinOffLine($http,$scope,checkindata,arrayid) {
    
    // So send the checkin to the back end server
    $http({
        method  : 'POST',
        url     : baseurl + "/api/visitor/checkin",
        headers: {
            "X-Auth-Token": user.access_token,   //If your header name has spaces or any other char not appropriate
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data    : $.param({first_name: checkindata.first_name
                                , car_reg: checkindata.car_reg
                                , last_name: checkindata.last_name
                                , company_representing: checkindata.company_representing
                                , email: checkindata.email
                                , company_id: checkindata.company_id
                                , title: checkindata.title
                                , travel_type: checkindata.travel_type
                                , optin: checkindata.optin
                                , vistor_company : checkindata.checkin_company
                                , checkin_time: checkindata.checkin_time
                                , checkin_categoryId : checkindata.checkin_categoryId
                                , checkin_categoryName : checkindata.checkin_categoryName
                                , checkin_badgeno : checkindata.checkin_badgeno
                                , checkin_signature_accepted : checkindata.checkin_signature_accepted
                                , checkin_signature : checkindata.checkin_signature



        }),  // pass in data as strings
        dataType: 'json'
    }).success(function(data) {
        checkin = data.results;
        if (debug ) { console.log(checkin); }
        checkincache.splice(arrayid, 1);
    }).error(function (error, status){
        console.error({ message: error, status: status});
        isOnline = false;         
    });    
 
 
}



/*

Send lost badges that are passed

*/


function sendlostBadgeOffLine($http,$scope,checkindata,arrayid) {
    
    // So send the checkin to the back end server
    $http({
        method  : 'POST',
        url     : baseurl + "/api/visitor/lostbadge",
        headers: {
            "X-Auth-Token": user.access_token,   //If your header name has spaces or any other char not appropriate
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data    : $.param({fullname: checkindata.name
                                , badger_no: checkindata.badge
                                , manager: checkindata.manager
                                , manager_id: checkindata.managerid
                                , company: checkindata.department
                                , departmentid: checkindata.departmentid
                                , checkin_time: checkindata.checkin_time
        }),  // pass in data as strings

        dataType: 'json'
    }).success(function(data) {
        lostbadgedata = data.results;
        if (debug ) { console.log(lostbadgedata); }
        lostbadgecache.splice(arrayid, 1);
    }).error(function (error, status){
        console.error({ message: error, status: status});
        isOnline = false;         
    });    
 
 
}



/* 

	Pull all the defaults for this user 

*/


function getSystemData($http) {
    


        //
        // Get categories if turned on
        //

        if (user.category == "Y") {

            $http({
                method  : 'GET',
                url     : baseurl + "/api/category",
                headers: {
                    "X-Auth-Token": user.access_token,   //If your header name has spaces or any other char not appropriate
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                dataType: 'json'
            }).success(function(data) { 
                categories = data.results;
                if (debug ) { console.log(categories); }

                // Adjust the flag is needed
                if (user.category == 'Y') {
                    if (categories) {
                        if (categories.length > 0 ) { showcategoriespage = true; }   
                    }
                }                  


                isOnline = true;
            }).error(function (error, status){
                console.error({ message: JSON.stringify(error), status: JSON.stringify(status)});
                console.error({ message: '/api/category --- Failed'});
                isOnline = false;                     
            });      


        }


        //
        // Get the company information
        //

        
        $http({
            method  : 'GET',
            url     : baseurl + "/api/company",
            headers: {
                "X-Auth-Token": user.access_token,   //If your header name has spaces or any other char not appropriate
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            dataType: 'json'
        }).success(function(data) {
            company = data.results;
            
            // Loop through the companies and any staff then set the flag.
            for (var i=0;i < company.length;i++)
            {
            
                if (company[i].rectype == 'staff') {
                    hasStaff = true;
                }
            }
            
            isOnline = true;
            
            if (debug ) { console.log(company); }
        }).error(function (error, status){
            console.error({ message: JSON.stringify(error), status: JSON.stringify(status)});
            console.error({ message: '/api/company --- Failed'});
            isOnline = false;                    
        });    

        //
        // Get vistors
        //

        $http({
            method  : 'GET',
            url     : baseurl + "/api/visitor",
            headers: {
                "X-Auth-Token": user.access_token,   //If your header name has spaces or any other char not appropriate
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            dataType: 'json'
        }).success(function(data) {
            vistors = data.results;
            if (debug ) { console.log(data.results); }
            if (debug ) { console.log(vistors); }
            isOnline = true;
        }).error(function (error, status){
            console.error({ message: JSON.stringify(error), status: JSON.stringify(status)});
            console.error({ message: '/api/visitor --- Failed'});
            isOnline = false;                   
        }); 

         //
        // Get Already Registered / Prefilled Visitors
        //
        if ( user.enable_prefill == 'Y') {        
             $http({
                method  : 'GET',
                url     : baseurl + "/api/visitor/getprefilled",
                headers: {
                    "X-Auth-Token": user.access_token,   //If your header name has spaces or any other char not appropriate
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                dataType: 'json'
            }).success(function(data) {
                prefilledvisitors = data.results;
                if (debug ) { console.log('test prefilled'); console.log(prefilledvisitors); }
                isOnline = true;
            }).error(function (error, status){
                console.error({ message: JSON.stringify(error), status: JSON.stringify(status)});
                console.error({ message: '/api/visitor/getprefilled --- Failed'});
                isOnline = false;                
            }); 
        }        
        
        //
        // Get previous names
        //
        
        $http({
            method  : 'GET',
            url     : baseurl + "/api/visitor/prevnames",
            headers: {
                "X-Auth-Token": user.access_token,   //If your header name has spaces or any other char not appropriate
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            dataType: 'json'
        }).success(function(data) { 
            prevnames = data.results;
            if (debug ) { console.log(prevnames); }
            isOnline = true;
        }).error(function (error, status){
            console.error({ message: JSON.stringify(error), status: JSON.stringify(status)});
            console.error({ message: '/api/visitor/prevnames --- Failed'});
            isOnline = false;
        });      

        //
        // Get the department information
        //

        if ( user.lostbadge == 'Y') {
        
            $http({
                method  : 'GET',
                url     : baseurl + "/api/department",
                headers: {
                    "X-Auth-Token": user.access_token,   //If your header name has spaces or any other char not appropriate
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                dataType: 'json'
            }).success(function(data) {
                departments = data.results;

                isOnline = true;
                
                if (debug ) { console.log(company); }
              }).error(function (error, status){
                console.error({ message: JSON.stringify(error), status: JSON.stringify(status)});
                isOnline = false;
            });    

        }        
        
    

}




/*

	Function to handle screen scrolling except if the class scrollable is on the element and then it can be scrolled ok

*/

var handleMove = function (e) {
    var scrollable = false;
    var items = $(e.target).parents();
    $(items).each(function(i,o) {
        if($(o).hasClass("scrollable")) {
            scrollable = true;
        }
    });
    if(!scrollable)
        e.preventDefault();
};


/*
    Function to restart app
*/

function restartApp(appenduser) {

    if (appenduser === undefined)
    {
        appenduser = true;
    }



    if ( appenduser ) {
        if (isOnline) {
            window.location = initialHref + "?username=" + savedusername + "&password=" + savedpassword + "&printer=" + savedprinter;
        }
    }
    else
    {
        window.location = initialHref;
    }

}

/*
	Function to pull the params from the base url , this is used on a autmatic restart 
*/

function getQueryParams(paramname) {

    var retStr = "";

    if (location.search) {
        var parts = location.search.substring(1).split('&');

        for (var i = 0; i < parts.length; i++) {
            var nv = parts[i].split('=');
            if (!nv[0]) continue;
            if ( nv[0] == paramname ) {
                retStr =  nv[1];   
            }
        }
    }

    return(retStr);
}