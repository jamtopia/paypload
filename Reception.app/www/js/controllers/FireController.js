// This is a JavaScript file

myApp.controller('fireController', function($scope,$http,$interval,$rootScope) {
    
    $scope.checkin_name = "";
    $scope.company_name = user.terminal_name;

    $scope.test = "";
    $scope.contButtonDisabled = true; 
    
    
    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;    

    $scope.items = [];    
    $scope.buttondisbled = "";    
    
    $scope.noneleft = false;

    $scope.init = function () {
        $('#checkout_name').focus();      

        $rootScope.rootbgVideoShow = false;


        //
        // Offline message 
        //
        if ( !isOnline ) {
            ons.notification.alert({
                message: 'No WiFi detected. Please visit your dashboard and view your fire escape list there to ensure complete accuracy of visitors in the building.<br/><br/><a href="' + baseurl + '">Press to load dashboard</a>'
            });
        }

        //
        // Get vistors
        //

        $http({
            method  : 'GET',
            url     : baseurl + "/api/visitor",
            headers: {
                "X-Auth-Token": user.access_token,   //If your header name has spaces or any other char not appropriate
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            dataType: 'json'
        }).success(function(data) {
            vistors = data.results;
    
            $scope.refreshVisitorList();


        }).error(function (response) {
            $scope.noneleft = true;
        });         

       
    
    };


    $scope.refreshVisitorList = function() {

            $scope.noneleft = true;

            $scope.items = [];    


            // Popuklate the list of people still in the building#
            for (var i=0;i < vistors.length;i++)
            {
                
                var compname = "";
                if (vistors[i].company_representing != '') {
                    compname = vistors[i].company_representing + " : ";
                }
                
                
                obj = {
                    id: vistors[i].id,
                    name: compname + vistors[i].first_name + " " + vistors[i].last_name
                };

                $scope.items.push(obj);
                
                $scope.noneleft = false;

            }             
    };


    $scope.redirectBack = function() {

        $rootScope.rootbgVideoShow = true;
        $('#spinner').hide();
        myNavigator.popPage();

    };

    $scope.$on('$destroy', function(){
        //$interval.cancel(localpromise);
    });    
    
    
    // $scope.$broadcast('FORM_ERROR');
    $scope.checkout = function(id) {
        
        $('#spinner').show();
        $scope.buttondisbled = "true";
        
            saveCheckout(id,false);

            //
            // Get vistors
            //
            
            for (var i=0;i < vistors.length;i++)
            {
                if (id ==  vistors[i].id) {
                    vistors.splice(i, 1);
                }
            }
            
            $scope.refreshVisitorList();

            if (!checkoutactive) {
                processCachedOut($http,$scope);     
            }              

            $('#spinner').hide();
    

    };
    
    $scope.checkfinish = function() {
        patient = $('#checkin_name').val();
        myNavigator.replacePage('thankscheckout.html');
    };        
 
});


