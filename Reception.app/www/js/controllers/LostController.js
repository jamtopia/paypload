// This is a JavaScript file

myApp.controller('lostController', function($scope,$http,$interval,$rootScope) {

    $scope.lost_name = "";
    $scope.lost_badge = "";
    $scope.lost_manager = "";       
    $scope.lost_department = "";

    $scope.lastmanagername = "";
    $scope.lastmanagerid = 0;

    $scope.lastdepartment = "";
    $scope.lastdepartmentid = 0;

    $scope.formOK = false;
    
    var localpromise;

    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;    
    $scope.checkinbgcolor = user.checkin_bgcolor;
    $scope.checkoutbgcolor = user.checkout_bgcolor;    
    $scope.checkoutcolor = user.checkout_color;  

    $scope.checkincolor = user.checkin_color;        
    $scope.bgimage = baseurl + "/" + user.background_image;   
    $scope.buttontext = 'Please Press Here To Check In';
    
    $scope.init = function () {

        $rootScope.rootbgVideoShow = false;

        $('#didyoumeanmanager').hide();
        $('#didyoumeandept').hide();

        $scope.company_name = user.terminal_name;
        $scope.company_image = baseurl + "/" + user.logo_path;         

        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);  

        $interval($scope.focus, 100, 1);  

    };

    $scope.focus = function() {
        $('#lost_name').focus();
    };


    $scope.redirectBack = function() {

        $rootScope.rootbgVideoShow = true;
        $('#spinner').hide();

        $interval.cancel(localpromise);


        $(document.activeElement).filter(':input:focus').blur();            
        $('#lost_name').blur();

        myNavigator.popPage();

    };


    $scope.$on('$destroy', function(){
        $interval.cancel(localpromise);
    });    
    


    $scope.watchEnter = function(keyEvent,step) {

        // Reset the timer if a key is pressed
        $interval.cancel(localpromise);
        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);          


        if (keyEvent.which === 13)
        {
            if ( step == 1) {
                $('#lost_badge').focus();
            }
            if ( step == 2) {
                $('#lost_manager').focus();
            }
            if ( step == 3) {
                $('#lost_department').focus();
            }
            if ( step == 4) {
                //$(document.activeElement).filter(':input:focus').blur();              
            }

        }    
    };
    



    $scope.changecheckin = function(jumptofirsterror) {

        if (jumptofirsterror === undefined) {
            jumptofirsterror = false;
        }

        
        $scope.formOK = true;

        $('#nameTick').hide();
        $('#nameCross').hide();
        $('#badgeTick').hide();
        $('#badgeCross').hide();
        $('#managerTick').hide();
        $('#managerCross').hide();
        $('#departmentTick').hide();
        $('#departmentCross').hide();

        var checkinnameVal = $('#lost_name').val();
        var checkinbadgeVal = $('#lost_badge').val();
        var checkinmanagerVal = $('#lost_manager').val();
        var checkindepartmentVal = $('#lost_department').val();
       
        
        if (checkinnameVal === "") {
            $scope.formOK = false;
            $('#nameCross').show();
            $('#nameTick').hide();

        }
        else
        {
            $('#nameCross').hide();
            $('#nameTick').show();
        }
 
        if (checkinbadgeVal === "") {
            $scope.formOK = false;
            $('#badgeCross').show();
            $('#badgeTick').hide();            
        }
        else
        {
            $('#badgeCross').hide();
            $('#badgeTick').show();
        }
        
 
        if (checkinmanagerVal === "") {
            $scope.formOK = false;
            $('#managerCross').show();
            $('#managerTick').hide();            
        }
        else
        {

            //So it's blank lets start looking for the managers name from the companies list
    
            matchcount = 0;
            lastid = -1;
            $scope.lastmanagername = "";
            $scope.lastmanagerid = 0;

            $('#didyoumeanmanager').hide();

            for (var i=0;i < company.length;i++)
            {
            
                if (company[i].rectype == 'normal') {

                    if (checkinmanagerVal.length > 1) {

                        if (company[i].name.replace(/ {2,}/g, ' ').toLowerCase().indexOf(checkinmanagerVal.toLowerCase()) === 0 )
                        {
                            $scope.lastmanagername = company[i].name;
                            $scope.lastmanagerid = company[i].id;
                                
                            matchcount++;
                        }
                    }
                }
    
            }   
                
            if (matchcount > 0) {
                $('#didyoumeanmanager').show();
            }

            $('#managerCross').hide();
            $('#managerTick').show();
        }

 
        if (checkindepartmentVal === "") {
            $scope.formOK = false;
            $('#departmentCross').show();
            $('#departmentTick').hide();            
        }
        else
        {

            //So it's blank lets start looking for the managers name from the companies list
    
            matchcount = 0;
            lastid = -1;
            $scope.lastdepartment = "";
            $scope.lastdepartmentid = 0;

            $('#didyoumeandept').hide();

            for (var i=0;i < departments.length;i++)
            {
            
                if (checkindepartmentVal.length > 1) { 

                    if (departments[i].name.replace(/ {2,}/g, ' ').toLowerCase().indexOf(checkindepartmentVal.toLowerCase()) === 0 )
                    {
                        $scope.lastdepartment = departments[i].name;
                        $scope.lastdepartmentid = departments[i].id;
                            
                        matchcount++;
                    }
                }
    
            }   
                
            if (matchcount > 0) {
                $('#didyoumeandept').show();
            }


            $('#departmentCross').hide();
            $('#departmentTick').show();
        }                
        
        $('#lost_name').val(toTitleCase($('#lost_name').val()));
        $('#lost_manager').val(toTitleCase($('#lost_manager').val()));
        $('#lost_department').val(toTitleCase($('#lost_department').val()));

 
    };       



    $scope.selectManagerMatch = function() {

        $scope.lost_manager = $scope.lastmanagername;
        $('#lost_department').focus();

    };    
    
    $scope.selectDeptMatch = function() {

        $scope.lost_department= $scope.lastdepartment;
        $('#lost_department').blur();

    };    


    
    $scope.checkfinish = function() {
        
        $scope.buttondisbled = "";

        $scope.formOK = true;
        
        $scope.changecheckin(true);
    
        if ($scope.formOK) {
            
            $('#spinner').show();
            saveLostBadge($scope.lost_name,$scope.lost_badge ,$scope.lost_manager,$scope.lastmanagerid ,$scope.lost_department,$scope.lastdepartmentid);

        }
        else
        {
            //$('#checkin_firstname').focus();
        }

    };       
    

    
    function toTitleCase(str)
    {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
    

});


