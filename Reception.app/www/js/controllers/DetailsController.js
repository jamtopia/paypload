// This is a JavaScript file

myApp.controller('detailsController', function($scope,$http,$interval,$rootScope) {

    $scope.checkin_name = patient;
    $scope.company_name = "";
    $scope.company_image = "";       
    $scope.optin = false;
    $scope.prev_name = "";
    $scope.dialog = undefined;
    
    $scope.formOK = false;

    $scope.emailalert;
    
    var localpromise;
    var localpromisecontinue;    

    var pf_firstname = "";    
    var pf_surnamename = "";    
    var pf_email = "";    
    var pf_optin = "";    
    var pf_company = "";    

    var autofillOnce = true;

    var emailalertid = 'emailalert' + Math.floor( Date.now() / 1000 );
    
    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;    
    $scope.checkinbgcolor = user.checkin_bgcolor;
    $scope.checkoutbgcolor = user.checkout_bgcolor;    
    $scope.checkoutcolor = user.checkout_color;  
    $scope.terminal_name = user.terminal_name

    $scope.checkincolor = user.checkin_color;        
    $scope.bgimage = baseurl + "/" + user.background_image;   
    $scope.buttontext = 'Please Press Here To Check In';
    
    $scope.init = function () {

        $rootScope.rootbgVideoShow = false;

        company_override_message = '';

        $scope.company_name = user.terminal_name;
        $scope.company_image = baseurl + "/" + user.logo_path;         

        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);  

        $interval($scope.focus, 100, 1);  

        if (user.hide_email_checkin == 'Y')
        {
            $('#entryemail').hide();
            $('#entryemailusedata').hide();
            
        }

        if (checkin_category != undefined)
        {
            if (checkin_category.hide_email_checkin == 'Y')
            {
                $('#entryemail').hide();
                $('#entryemailusedata').hide();                
            }
        }


        if (user.hide_company_checkin == 'Y')
        {
            $('#entrycompany').hide();
            $('#entrycompanyusedata').hide();
            
        }

        if (checkin_category != undefined)
        {
            if (checkin_category.hide_company_checkin == 'Y')
            {
                $('#entrycompany').hide();
                $('#entrycompanyusedata').hide();                
            }
        }
        
        // Change to optional text if vlidation is needed
        if (user.validate_car_reg == 'Y')
        {
            $('#checkin_reg').attr('placeholder','Please Enter Your Car Registration*'); 
        }
        
        if (user.validate_email == 'Y')
        {
            $('#checkin_email').attr('placeholder','Please Enter Your Email Address*');
        }        
        
        if (user.reverse == 'Y') { 
            $scope.buttontext =  'Press Here to Continue';
        }

    };


    $scope.focus = function() {
        $('#checkin_firstname').focus();
    };

    $scope.redirectBack = function() {

        $rootScope.rootbgVideoShow = true;
        $('#spinner').hide();
        $interval.cancel(localpromise);


        $(document.activeElement).filter(':input:focus').blur();            
        $('#checkin_firstname').blur();
        myNavigator.popPage();
    };

    $scope.$on('$destroy', function(){

        $scope.emailalert = document.getElementById(emailalertid);

        if ($scope.emailalert) { $scope.emailalert.hide() }


        $interval.cancel(localpromise);
        $interval.cancel(localpromisecontinue);
    });    
    

    
    
    $scope.checkfinish = function() {
        
        $scope.buttondisbled = "";

        $scope.formOK = true;
        
        $scope.changecheckin(true);
    
        if ($scope.formOK) {
            
            $('#spinner').show();
            $scope.buttondisbled = "true";          

            checkin_firstname = $('#checkin_firstname').val();
            checkin_reg = $('#checkin_reg').val();
            checkin_surname = $('#checkin_surname').val();
            checkin_email = $('#checkin_email').val();
            checkin_mobile = $('#checkin_mobile').val();    
            checkin_company = $('#checkin_company').val();    
            //get_here =  $('#get_here').val();   
            optin = $scope.optin;

            if (user.hands == 'Y' && checkin_email === "" && (user.attachement_path.split('.').pop() == 'jpg') ) {   
                $(document.activeElement).filter(':input:focus').blur();     
                $interval.cancel(localpromise);                
                $('#spinner').hide();              

                if(angular.equals(checkin_categoryName, "Contractor") && user.advancedcontractor == 'Y'){
                    myNavigator.replacePage('contractor.html', { animation : 'none' });                            
                }        
                else if(checkin_ndaEnabled == 'Y'){
                    myNavigator.replacePage('contractor.html', { animation : 'none' });                            
                }                        
                else
                {            
                    myNavigator.replacePage('hands.html', { animation : 'none' });          
                }
            }
            else
            {

                if (checkin_categoryVistorRequest) { 

                    if (user.reverse == 'Y') { 

                        $interval.cancel(localpromise);                        
                        $('#spinner').hide();


                        if(angular.equals(checkin_categoryName, "Contractor") && user.advancedcontractor == 'Y'){
                            myNavigator.replacePage('contractor.html', { animation : 'none' });                            
                        }
                        else if(checkin_ndaEnabled == 'Y'){
                            myNavigator.replacePage('contractor.html', { animation : 'none' });                            
                        }
                        else
                        {
                            myNavigator.replacePage('checkin.html', { animation : 'none' });                            
                        }

                        
                    }
                    else
                    { 
                        $interval.cancel(localpromise);
                        $('#spinner').show();                    
                        saveCheckin();                
                    }
                }
                else
                {
                    // So they have gone through the category screen and selected a category that needs just to be let in so just check them in

                    if(angular.equals(checkin_categoryName, "Contractor") && user.advancedcontractor == 'Y'){

                        $interval.cancel(localpromise);
                        $('#spinner').hide();                    

                        myNavigator.replacePage('contractor.html', { animation : 'none' });                            
                    }
                    else if(checkin_ndaEnabled == 'Y'){
                        
                        $interval.cancel(localpromise);
                        $('#spinner').hide();                              
                        myNavigator.replacePage('contractor.html', { animation : 'none' });                            
                    }
                    else
                    {
                        patient = '';
                        patient_id = -1;
                        get_here =  '';
                        checkin_reg = ''; 

                        $interval.cancel(localpromise);
                        $('#spinner').show();                    
                        saveCheckin();                       
                    }
                }

            }

        }
        else
        {
            //$('#checkin_firstname').focus();
        }

    };       
    

    $scope.watchEnter = function(keyEvent,step) {

        // Reset the timer if a key is pressed
        $interval.cancel(localpromise);
        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);          


        if (keyEvent.which === 13)
        {
            if ( step == 1) {
                $('#checkin_surname').focus();
            }
            if ( step == 2) {
                if (user.apptype == 'custr') {
                    $('#checkin_email').focus();
                }
                else
                {
                    $('#checkin_email').focus();
                }
            }
            if ( step == 3) {
                $('#checkin_company').focus();
            }
            if ( step == 4) {
                //$(document.activeElement).filter(':input:focus').blur();              
            }

        }    
    };
    
    $scope.changecheckin = function(jumptofirsterror) {

        if (jumptofirsterror === undefined) {
            jumptofirsterror = false;
        }

        // Reset the timer for the conitue button
        $interval.cancel(localpromisecontinue);
        
        $scope.formOK = true;
        //
        // Loop the previous names and if they match the enter for them if there is nothing in the email
        //
        if ( $('#checkin_email').val() === ""  ) {
            matchcount = 0;
            lastid = -1;

            $('#didyoumean').hide();

            if ( $('#checkin_surname').val() !== '' ) {

                if (prevnames) {

                    for (var i=0;i < prevnames.length;i++)
                    {
                        
                        var nameToCheck = prevnames[i].first_name.toLowerCase() + " " + prevnames[i].last_name.toLowerCase();
                        nameToCheck = nameToCheck.replace(/\s\s+/g, ' ');

                        if (nameToCheck.toLowerCase() == ($('#checkin_firstname').val().toLowerCase() + " " + $('#checkin_surname').val().toLowerCase()) ){
                            matchcount = matchcount + 1;
                            lastid = i;
                        }
            
                    }            
                }

            }
                
            if (matchcount == 1) {

                pf_firstname = prevnames[lastid].first_name;  
                pf_surnamename = prevnames[lastid].last_name;   
                pf_email = prevnames[lastid].email;   
                pf_optin = prevnames[lastid].OPTIN;
                pf_car_reg = prevnames[lastid].car_reg;
                pf_company = prevnames[lastid].vistor_company;  


                $scope.prev_name = pf_firstname + " " + pf_surnamename;
                $('#didyoumean').show();
                autofillOnce = false;

            }
        }        

        $('#emailCross').hide();
        $('#emailTick').hide();
        $('#firstnameCross').hide();
        $('#firstnameTick').hide();
        $('#surenameCross').hide();
        $('#surenameTick').hide();
        $('#companyCross').hide();
        $('#companyTick').hide();



        var checkinfirstnameVal = $('#checkin_firstname').val();
        var checkinsurnameVal = $('#checkin_surname').val();
        var checkinemailVal = $('#checkin_email').val();
        var checkincompanyVal = $('#checkin_company').val();
       
        
        if (checkinfirstnameVal === "") {
            $scope.formOK = false;
            $('#firstnameCross').show();
            $('#firstnameTick').hide();

        }
        else
        {
            $('#firstnameCross').hide();
            $('#firstnameTick').show();
        }
 
        if (checkinsurnameVal === "") {

            if ( $scope.formOK ) { 
                if (jumptofirsterror) { 

                    $('#checkin_surname').focus(); 
                    step=1; 
           
                }
            }
            $scope.formOK = false;
            $('#surenameCross').show();
            $('#surenameTick').hide();
            
        }
        else
        {
            $('#surenameCross').hide();
            $('#surenameTick').show();
        }

        if (user.apptype =='custr') {
            if (checkincompanyVal === "") {

                if ( $scope.formOK ) { 
                    if (jumptofirsterror) { 

//                        $('#checkin_company').focus(); 
                        step=2;                 
                    }
                }                

                //$scope.formOK = false;
                $('#companyCross').hide();
                $('#companyTick').hide();
            }
            else
            {
                $('#companyCross').hide();
                $('#companyTick').hide();
            }
        }

        if (checkinemailVal !== "") {

            if( !validateEmail(checkinemailVal)) {
                if ( $scope.formOK ) { 
                    if (jumptofirsterror) { 

                        $('#checkin_email').focus(); 
                        step=3; 

                    }
                }                
                $scope.formOK = false;
                $('#emailCross').show();
                $('#emailTick').hide();
            }
            else
            {
                $('#emailCross').hide();
                $('#emailTick').show();
                
            }
        }
        else {


            if (checkin_category != undefined)
            {
                if (checkin_category.hide_email_checkin != 'Y')
                {

                    if (user.validate_email == 'Y')
                    {
                        if ( $scope.formOK ) { 
                            if (jumptofirsterror) { 

                                $('#checkin_email').focus(); 
                                step=3; 
                            }
                        }                 
                        $scope.formOK = false;
                        $('#emailCross').show();
                        $('#emailTick').hide();
                    }
                }
            }
        }


        
        $('#checkin_firstname').val(toTitleCase($('#checkin_firstname').val()));
        $('#checkin_surname').val(toTitleCase($('#checkin_surname').val()));
        $('#checkin_company').val(toTitleCase($('#checkin_company').val()));
        $('#checkin_email').val($('#checkin_email').val().toLowerCase());

        localpromisecontinue = $interval($scope.showtopCheck, 5000, 1);          

 
    };       


    $scope.showtopCheck = function() {
        $('#topcheckinName').show();
    };

    $scope.selectContinue = function() {
        $('#checkin_firstname').blur();    
        $('#topcheckinName').hide();                  
    };
    


    $scope.selectMatch = function() {

        $('#checkin_firstname').val(pf_firstname);
        $('#checkin_surname').val(pf_surnamename);
        $('#checkin_email').val(pf_email);
        $('#checkin_company').val(pf_company);

        if ( pf_optin == 'false') {
            $scope.optin = false;
        }
        else
        {
            $scope.optin = true;
        }

        $scope.changecheckin();

        // Lose the keyboard
        $(document.activeElement).filter(':input:focus').blur();   
    };   
    
    function validateEmail($email) {
        var emailReg = /^([\w-\.\+]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }    
    
    function toTitleCase(str)
    {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
    
    $scope.optinclick = function() {

        if ( $('#optin').html() == 'OPT-IN') {
            $('#optin').html('OPT-OUT');
            $('#optin').attr('style','color: white; background-color: lightgray;');
            $scope.optin = false;
        }
        else
        {
            $('#optin').html('OPT-IN');
            $('#optin').attr('style','color: ' + $scope.checkincolor +'; background-color: ' + $scope.checkinbgcolor +';');
            $scope.optin = true;
        }
    };
    
    $scope.optinclickv2 = function() {

        if ( !$scope.optin ) {
            $scope.optin = true;
        }
        else
        {
            $scope.optin = false;
        }
    };    
    
    $scope.gethere = function(gethere) {
        $('#get_here').val(gethere);
        
        if (gethere == 'car') {
            $('#howdidyougethere').hide(); 
            $('#carregdiv').show();
            $('#checkin_reg').focus();
        }
        
        $('.iconitem').css('color',$scope.textcolor);
        //$('.iconitem').css('text-shadow',$scope.textbgcolor);

        // Set the selected on
        $('#' + gethere + 'icon').css('color','#7CFC00');
        //$('#' + gethere + 'icon').css('text-shadow',$scope.textcolor);
        
        $scope.changecheckin();
        
    };
    
    $scope.changeTransport = function(){
        $('.iconitem').css('color',$scope.textcolor);
        $('#get_here').val('');
        $('#howdidyougethere').show(); 
        $('#carregdiv').hide();            
    };
    
    $scope.emailinfo = function() {
        
        
        var alertmessage = 'I understand that by entering my details, I am providing  ' + user.terminal_name + ' with my personal data. By clicking “Opt-in”, I consent to this personal data being used by the ' + user.terminal_name + ' for the purpose of contacting me in the future by email about their products or services. I also understand and agree that my personal data may be held and used by the owner of this data capture App, MediaBase Direct Ltd, in order to run the App and for analysing visitor traffic with a view to improving the App experience for visitors and others. You will receive an email with ' + user.terminal_name + ' fire/emergency procedure and privacy policy where you can see how ' + user.terminal_name + ' will use your data.';

        
        if (user.disclaimer_wording) {
            var alertmessage = user.disclaimer_wording;

        }


       
        
        ons.notification.alert({
          message: alertmessage,
          // or messageHTML: '<div>Message in HTML</div>',
          title: 'Information',
          buttonLabel: 'OK',
          id: emailalertid
        });



    };
    
    $scope.carinfo = function() {
        ons.notification.alert({
          message: 'The management cannot accept responsibility for loss or damage of property or Vehicles whilst visiting us today.',
          // or messageHTML: '<div>Message in HTML</div>',
          title: 'Information',
          buttonLabel: 'OK'
        });
    };

});


