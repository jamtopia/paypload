// This is a JavaScript file

myApp.controller('staffController', function($scope,$http,$interval,$rootScope) {
    
    $scope.checkin_name = "";
    $scope.company_name = "";
    $scope.representative_mobile = "";
    $scope.direction = "";
    $scope.checkout_id=0;
   
    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;    
    $scope.company_name = user.terminal_name;
    

    $scope.bgimage = "img/blank.png";   
    if ( user.background_image !== undefined ) {
        $scope.bgimage = baseurl + "/" + user.background_image; 
    }
       
    $scope.company_image = "img/blank.png";   
    if ( user.logo_path !== undefined ) {
        $scope.company_image = baseurl + "/" + user.logo_path;
    }
    
    var localpromise;

    $scope.init = function () {

        $rootScope.rootbgVideoShow = false;

        $interval($scope.focus, 100, 1);  

        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);
        
    }; 
    
    $scope.selectPatient = function() {
    
        $('#checkin_name').val($scope.checkin_name);
        
        patient = $('#checkin_name').val();
        patient_id = $scope.checkin_id;
        
        $('#spinner').show();
        
        if ($scope.direction == 'Check In') {
            // So send the checkin to the back end server
            $http({
                method  : 'POST',
                url     : baseurl + "/api/visitor/checkin",
                headers: {
                    "X-Auth-Token": user.access_token,   //If your header name has spaces or any other char not appropriate
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data    : $.param({first_name: patient
                                        , car_reg:''
                                        , last_name:'Staff'
                                        , company_representing: patient
                                        , email:''
                                        , company_id: patient_id
                                        , title : ''      
                                        , optin : 'false'
                }),  // pass in data as strings
                dataType: 'json'
            }).success(function(data) {
                checkin = data.results;
                getSystemData($http);
                if (debug ) { console.log(checkin); }
                $scope.redirectBack();
            }).error(function (response) {
                /* display proper error message */
                ons.notification.alert({
                    message: 'Sorry we have been unable to check you in.'
                });
                $('#spinner').hide();
                $scope.buttondisbled = "";
                
            });            
                     
        }
        else
        {
            $('#spinner').show();
            $scope.buttondisbled = "true";
            
            $http({
                method  : 'POST',
                url     : baseurl + "/api/visitor/checkout/" + $scope.checkout_id ,
                headers: {
                    "X-Auth-Token": user.access_token,   //If your header name has spaces or any other char not appropriate
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                dataType: 'json'
            }).success(function(data) {
                checkout = data.results;
                getSystemData($http);
                if (debug ) { console.log(checkout); }
                
                $scope.redirectBack();
            }).error(function (response) {
                    /* display proper error message */
                    ons.notification.alert({
                        message: 'sorry we have had a problem checking you out.'
                    });
                    $('#spinner').hide();
                    $scope.buttondisbled = "";
                    
            });               
        }

    };       


    $scope.focus = function() {
        $('#checkin_name').focus();
    };        
    
    
    $scope.changecheckin = function() {

        var showresult = false;
        var nocompanies = true;
        var exactmatch = false;
        var searchVal = $('#checkin_name').val();
        
       
        // First find the matching staff member
        
        if (searchVal === "" || searchVal.length < 3) {
            showresult = false;
        }
        else
        {
            
            var matchcount = 0;
            
            for (var i=0;i < company.length;i++)
            {
            
                if (company[i].rectype == 'staff') {

                    if (debug) {
                        console.log('-----');
                        console.log(company[i]);
                    }
                    
                    $scope.contButtonDisabled = true;
                    if (company[i].name.toLowerCase().indexOf(searchVal.toLowerCase()) > -1 )
                    {
                        $scope.checkin_name = company[i].name;
                        $scope.checkin_id = company[i].id;
                        if ( company[i].representative_mobile !== '') {
                            $scope.representative_mobile = " ( " + company[i].representative_mobile + " ) ";
                        }
                        
                        matchcount++;
                        $scope.direction = "Check In";
                        showresult = true;
                    }

                }
            }
            

            
            // OK lets see if the staff is in the building already
            for (var i=0;i < vistors.length;i++)
            {
            
                if ((vistors[i].first_name + " " + vistors[i].last_name ).toLowerCase().indexOf($scope.checkin_name.toLowerCase()) === 0 ) {
                    $scope.direction = "Check Out";
                    $scope.checkout_id = vistors[i].id;
                }
            }                      
                
            
        }        
        
        
        if ( showresult ) {
            $('#checkinName').show();
        }
        else {
            $('#checkinName').hide();
        }        
        
    };
    
    $scope.redirectBack = function() {

        $rootScope.rootbgVideoShow = true;
        $('#spinner').hide();
        $interval.cancel(localpromise);


        myNavigator.popPage();

    };

    $scope.$on('$destroy', function(){
        $interval.cancel(localpromise);
    });    
    
    
    $scope.checkfinish = function() {
        patient = $('#checkin_name').val(); 
        patient_id = $scope.checkin_id;
        myNavigator.replacePage('details.html', { animation : 'none' });
    };        
    

});


