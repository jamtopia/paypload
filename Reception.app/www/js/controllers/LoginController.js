// This is a JavaScript file

myApp.controller('loginController', function($scope,$http,$interval,$sce) {
    
    $scope.buttondisbled = "";
    
    $scope.usernamedebug = usernamedebug;
    $scope.passworddebug = passworddebug;    
    $scope.padding = "";
    $scope.version = version;
    $scope.showRegister = showRegister;

    var usernameoverride = getQueryParams('username');
    var passwordoverride = getQueryParams('password');
    var savedprinter = getQueryParams('printer');
    
    if ( usernameoverride !== '') { $('#username').val(usernameoverride);}
    if ( passwordoverride !== '') { $('#password').val(passwordoverride);}

    if ( savedprinter !== '') {
        printerURL = savedprinter;
        hasPrinter = true;
    }

    
    $scope.init=function(){

        if ( passwordoverride !== '') { 

            $scope.authenticate(); 
        }        

    };

    $scope.registeruser = function() {
        $interval(myNavigator.resetToPage('register.html'), 3000, 1); 

    };

    $scope.authenticate = function() {
        
        
        if (  $('#username').val() !== "" && $('#password').val() !== "") 
        {
        
            $('#spinner').show();
            $scope.buttondisbled = "true";
            
            $http({
                method  : 'POST',
                url     : baseurl + "/api/login",
                data    : $.param({username: $('#username').val(), password: $('#password').val()}),  // pass in data as strings
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
            }).success(function(data) {
                
                
                if (data.error) {
                    ons.notification.alert({
                        message: 'Invalid Login'
                    });
                    $('#spinner').show();
                    $scope.buttondisbled = "";
                } else {
                    
                    user = data.results;

                    // see if the print is turned on and set flag
                    if ( user.label_printing == 'Y') {
                        printLabels = true;
                    }


                    savedpassword = $('#password').val();
                    savedusername = $('#username').val();
                    
                    if (debug) { console.log(user); }

                    if (user.apptype !== '') {
                        $('head').append('<link rel="stylesheet" href="css/' + user.apptype + '.css" type="text/css" />');
                    }

                    var colorcss = '<style>.textcolor { color: ' + user.text_color + ' !important; }';
                    colorcss = colorcss + 'input,password,.input-text {border-left:2px solid ' + user.checkin_bgcolor + ' !important; }';
                    colorcss = colorcss + '.errspan,.errspanred, .underlinecolour {color:' + user.checkin_bgcolor + ' !important; }';
                    colorcss = colorcss + '.textbgcolor { background-color: ' + user.text_bgcolor + ' !important; }';
                    colorcss = colorcss + '.checkinbgcolor { background-color: ' + user.checkin_bgcolor + ' !important; }';
                    colorcss = colorcss + '.checkincolor { color: ' + user.checkin_color + ' !important; }';
                    colorcss = colorcss + '.checkoutbgcolor { background-color: ' + user.checkout_bgcolor + ' !important; }';
                    colorcss = colorcss + '.checkoutcolor { color: ' + user.checkout_color + ' !important; } </style>';

                    $('head').append(  colorcss );  

                    if (user.font !== '') {

                        $('head').append('<style>* {' + user.fontcss + ' !important;} .button {' + user.fontcss + ' !important;} .fa { font-family: \'FontAwesome\' !important };');

                        var stylesheet = loadCSS( user.font);


                    }


                    getSystemData($http); 
                    
                    $('#spinner').hide();

                    if (printLabels) {

                        myNavigator.resetToPage('printer.html');

                    }
                    else
                    {

                        myNavigator.resetToPage('main.html');

                    }

    
                }
              }).error(function (data) {
                    /* display proper error message */

                if ( data === null ) {
                    ons.notification.alert({
                        message: 'Unable to contact server, please check the wifi'
                    });
                    $('#spinner').hide();
                    $scope.buttondisbled = "";                    
                } else  {
                    ons.notification.alert({
                        message: 'Invalid Login'
                    });
                    $('#spinner').hide();
                    $scope.buttondisbled = "";
                } 
                    
              });
        }
    };    

    
});