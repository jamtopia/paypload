// This is a JavaScript file

myApp.controller('thankscheckoutController', function($scope,$http,$interval,$rootScope) {


    $scope.checkin_name = patient;
    $scope.message = user.message;
    $scope.company_name = user.terminal_name;   
    
    
    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;   
    
    $scope.company_image = "img/blank.png";   
    if ( user.logo_path !== undefined ) {
        $scope.company_image = baseurl + "/" + user.logo_path;
    }    
    
    var localpromise;


    $scope.init=function(){
        
        getSystemData($http);
        localpromise = $interval($scope.redirectBack, thanksredirectdelay, 1);

    };

    
    $scope.redirectBack = function() {
        $rootScope.rootbgVideoShow = true;
        $('#spinner').hide();
        $interval.cancel(localpromise);        
        myNavigator.popPage();
    };


    $scope.$on('$destroy', function(){
        $interval.cancel(localpromise);
    });         
});    