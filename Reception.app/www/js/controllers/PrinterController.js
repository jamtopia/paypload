// This is a JavaScript file

myApp.controller('printerController', function($scope,$http,$interval,$rootScope) {

    $scope.checkin_name = patient;
    $scope.message = user.message;
    $scope.company_name = user.terminal_name;
    $scope.fire_image = 'img/blank.png';
    
    
    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;    

    $scope.company_image = "img/blank.png";   
    if ( user.logo_path !== undefined ) {
        $scope.company_image = baseurl + "/" + user.logo_path;
    }        
    
    var localpromise;       

    
    $scope.init=function(){

        $('#spinner').hide();        

        localpromise = $interval($scope.redirectBack, screenredirectdelay*5, 1);

        try {

            cordova.plugins.printer.check(function (available, count) {
                if ( available ) {
                    
                    cordova.plugins.printer.pick(function (url) {
                        printerURL = url;
                        hasPrinter = true;
                        myNavigator.resetToPage('main.html');                    
                    });                
                    
                }

            });                

        }
        catch(err) {
            hasPrinter = false;
            $interval($scope.redirectBack, 1000, 1);
        }

    };

    $scope.$on('$destroy', function(){
        $interval.cancel(localpromise);
    });  
    
    
    
    $scope.redirectBack = function() {
        $rootScope.rootbgVideoShow = true;
        $interval.cancel(localpromise);        
        myNavigator.resetToPage('main.html');
    };    
    
});    