// This is a JavaScript file

myApp.controller('categoriesController', function($scope,$http,$interval,$rootScope) {
    
    $scope.checkin_name = "";
    $scope.company_name = "";
    $scope.direction = "";
    $scope.checkout_id=0;
   
    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;    
    $scope.company_name = user.terminal_name;
    $scope.categories = categories;

    var localpromise;    

    $scope.bgimage = "img/blank.png";   
    if ( user.background_image !== undefined ) {
        $scope.bgimage = baseurl + "/" + user.background_image; 
    }
       
    $scope.company_image = "img/blank.png";   
    if ( user.logo_path !== undefined ) {
        $scope.company_image = baseurl + "/" + user.logo_path;
    }
    
    $scope.init = function () {

        $rootScope.rootbgVideoShow = false;

        checkin_categoryId = 0;
        checkin_categoryName = '';
        checkin_ndaEnabled = 'N';
        checkin_categoryVistorRequest = false;
        checkin_category = undefined;
        dbs_number = '';
        dbs_expire = '';   

        if (user.enable_prefill == 'Y') {
            if (user.enable_prefill_front_screen == 'N') {
                $('#tblcategory').show();
            }
            else
            {
                $('#tblcategory').hide();
            }
        }
        else
        {
            $('#tblcategory').hide();
        }


        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);
        
    }; 
    
    $scope.AlreadyRegCategory=function(){
        myNavigator.replacePage('prefilledcheckin.html', { animation : 'none' });
    };

    $scope.selectCategory = function (categoryId,categoryName,categoryVistorRequest,category) {

        checkin_categoryId = categoryId;
        checkin_categoryName = categoryName;
        checkin_ndaEnabled = category.nda_enabled;
        checkin_category = category;

        if ( categoryVistorRequest == 'Y') {
            checkin_categoryVistorRequest = true;    
        } else {
            checkin_categoryVistorRequest = false;                
        }
        
        // So they have selected catefgories so we will always go to the deails page next
        if(angular.equals(categoryName, "Contractor") && user.dbs_check == 'Y'){

            nextpage='dbs.html';                          
        }
        else if(angular.equals(categoryName, "Already Registered")){
            nextpage='prefilledcheckin.html';
        }
        else{
            nextpage = 'details.html';    
        }


        $('#spinner').hide();
        $interval.cancel(localpromise);

        myNavigator.replacePage(nextpage, { animation : 'none' });   


    };

    $scope.redirectBack = function() {
 
        $rootScope.rootbgVideoShow = true;
        $('#spinner').hide();
        $interval.cancel(localpromise);


        myNavigator.popPage();

    };

    $scope.$on('$destroy', function(){
        $interval.cancel(localpromise);
    });       

});


