// This is a JavaScript file

myApp.controller('PrefilledCheckinController', function($scope,$http,$interval,$rootScope) {
    $scope.prefilled_checkin_name = "";
    $scope.company_name = user.terminal_name;

    var lastmatch = "";
    var lastemail = "";
    var lastcheckintime = "";
    var lastpersonvisiting="";
    var checkingtime = "";
    var lastid = 0;    
    
    var prefilledcheckinid = -1;
    var checkout;
    
    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;    
    
    $scope.buttondisbled = "";    

    $scope.items = [];

    var localpromise;
    
    $scope.init = function () {

        $rootScope.rootbgVideoShow = false;
        
        $interval($scope.focus, 100, 1);  

        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);

    };


    $scope.focus = function() {
        $('#prefilled_checkin_name').focus();
    };    

    $scope.redirectBack = function() {

        $rootScope.rootbgVideoShow = true;
        $('#spinner').hide();
        $interval.cancel(localpromise);        
        $(document.activeElement).filter(':input:focus').blur();        
        $('#prefilled_checkin_name').blur(); 
        myNavigator.popPage();
    };

    $scope.$on('$destroy', function(){
        $interval.cancel(localpromise);
    });        
    
    $scope.changeprefilledcheckin = function() {

        $interval.cancel(localpromise);
        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);            

        var showresult = false;
        var exactmatch = false;
        var searchVal = $('#prefilled_checkin_name').val();
        
        var htmlResults = "";
        var maxshow = 0;
        
        $scope.items = [];
        
        if (searchVal !== "" ) {
            
            var matchcount = 0;
            var obj;
            var dupobj;        
            
            lastmatch = "";
            lastemail = "";
            lastcheckintime = "";
            checkingtime = "";
            lastpersonvisiting = "";
            lastid = 0;
            var duphit = false;

            if (prefilledvisitors) {

                for (var i=0;i < prefilledvisitors.length;i++)
                {
                    
      
                    checkingtime = prefilledvisitors[i].date_visiting;//.substring(11, 16);

                    var matchname = prefilledvisitors[i].first_name + " " + prefilledvisitors[i].last_name;
                    matchname = matchname.replace(/\s\s+/g, ' ');


                    if (( matchname  ).toLowerCase().indexOf(searchVal.toLowerCase()) === 0 ) {                

                        $scope.contButtonDisabled = false;
                        maxshow = maxshow +1;
                        
                        matchcount++;
                                           
                        lastmatch = prefilledvisitors[i].first_name + " " + prefilledvisitors[i].last_name;
                        lastemail = prefilledvisitors[i].email; 
                        lastcheckintime = checkingtime;
                        lastpersonvisiting = prefilledvisitors[i].person_visiting;
                        lastid = prefilledvisitors[i].id;

                        var showcompany = '';
                        var lastcompany = '';

                        if ( company[i].company_name) {
                            if ( company[i].company_name != '') {
                                showcompany = '- ' + company[i].company_name + '';                            
                                lastcompany = company[i].company_name + '';                            
                            }
                        }

                        obj = {
                            lastmatch: prefilledvisitors[i].first_name + " " + prefilledvisitors[i].last_name,
                            showcompany: showcompany,
                            lastcompany: lastcompany,
                            lastemail: prefilledvisitors[i].email,
                            lastcheckintime: checkingtime,
                            lastpersonvisiting: prefilledvisitors[i].person_visiting,
                            lastid: prefilledvisitors[i].id
                        };                     


                        console.log(prefilledvisitors);

                        $scope.items.push(obj);
                        
                    }
                }        

            }
            
            if (matchcount > 0 &&searchVal.length >= 2) {

                $scope.prefilled_checkin_name = lastmatch; //+ "(" + lastcheckintime + ")";
                $('#prefillpicker').show();     
                $('#notmatched').hide();     

            }
            else
            {
                if ( searchVal.length >= 2 ) {
                    $('#prefillpicker').hide();     
                    $('#notmatched').show();                     
                }
                else
                {
                    $('#prefillpicker').hide();     
                    $('#notmatched').hide();                         
                }
            }
            
            
        }
        else {
            $('#prefillpicker').hide();
            $('#notmatched').hide();     
        }
    };    
    
    $scope.prefilledcheckin = function() {

        patient = lastpersonvisiting;
        
        doPrefilledCheckin($http,prefilledcheckinid);

    };

    $scope.selectMatch = function() {

        prefilledcheckinid = lastid;
        //$('#prefilled_checkin_name').val(lastmatch + "(" + lastcheckintime + ")");
        $('#prefilled_checkin_name').val(lastmatch);
        $('#prefilledcheckinbutton').show();
        
        $('#didyoumean').hide();  
    };        

    $scope.selectMatchByID = function(item) {

        lastmatch = item.lastmatch;
        lastemail = item.lastemail
        lastcheckintime = item.lastcheckintime;
        lastpersonvisiting = item.lastpersonvisiting;
        lastid = item.lastid;

        prefilledcheckinid = lastid;

        $('#prefilled_checkin_name').val(lastmatch);
        $('#prefilledcheckinbutton').show();
        
        $('#prefillpicker').hide();  
    }
        

    
    $scope.checkfinish = function() {
        patient = $('#prefilled_checkin_name').val();
        $('#spinner').hide();
        $interval.cancel(localpromise);         
        myNavigator.replacePage('thanks.html');
    };        

    $scope.noMatchAction=function(){

        var nextpage = 'checkin.html';
        if (user.reverse == 'Y') { nextpage = 'details.html'; }
        if (showcategoriespage) {
            nextpage = 'categories.html';
        }  

        myNavigator.replacePage(nextpage, { animation : 'none' });
    };

});


