// This is a JavaScript file

myApp.controller('checkoutController', function($scope,$http,$interval,$rootScope) {
    
    $scope.checkin_name = "";
    $scope.checkout_name = "";
    $scope.company_name = user.terminal_name;

    $scope.test = "";
    $scope.contButtonDisabled = true; 

    var lastmatch = "";
    var lastemail = "";
    var lastcheckintime = "";
    var checkingtime = "";
    var lastid = 0;    
    var lastvisiting="";

    $scope.items = [];        
    
    var checkoutid = -1;
    var checkout;

    var localpromise;    
    
    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;    
    
    $scope.buttondisbled = "";    

    $scope.items = [];
    
    $scope.init = function () {

        $rootScope.rootbgVideoShow = false;
        
        $interval($scope.focus, 100, 1);  

        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);

        rating = '';
        postcode = '';
        distance = '';

        if (user.hide_smiles_checkin == 'Y')
        {
            $('#entrysmiles').hide();           
        }        

    };


    $scope.focus = function() {
        $('#checkout_name').focus();
    };    

    $scope.redirectBack = function() {


        $rootScope.rootbgVideoShow = true;
        $('#spinner').hide();
        $interval.cancel(localpromise);        
        $(document.activeElement).filter(':input:focus').blur();        
        $('#checkout_name').blur(); 
        myNavigator.popPage();
    };

    $scope.$on('$destroy', function(){
        $interval.cancel(localpromise);
    });        
    
    $scope.changecheckout = function() {

        $interval.cancel(localpromise);
        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);            

        var showresult = false;
        var exactmatch = false;
        var searchVal = $('#checkout_name').val();
        
        var htmlResults = "";
        var maxshow = 0;
        
        $scope.items = [];
        
        if (searchVal !== "" ) {
            
            var matchcount = 0;
            var obj;
            var dupobj;        
            
            lastmatch = "";
            lastemail = "";
            lastcheckintime = "";
            checkingtime = "";
            lastvisiting="";
            lastid = 0;
            var duphit = false;

            for (var i=0;i < vistors.length;i++)
            {
                
  
                checkingtime = vistors[i].checkin_time.substring(11, 16);

                var matchname = vistors[i].first_name + " " + vistors[i].last_name;
                matchname = matchname.replace(/\s\s+/g, ' ');



                console.log(( matchname ).toLowerCase().indexOf(searchVal.toLowerCase()));

                if (( matchname ).toLowerCase().indexOf(searchVal.toLowerCase()) >= 0 ) {                

                    $scope.contButtonDisabled = false;
                    maxshow = maxshow +1;


                    
                    matchcount++;
                                       
                    lastmatch = vistors[i].first_name + " " + vistors[i].last_name;
                    lastemail = vistors[i].email; 
                    lastcheckintime = checkingtime;
                    lastid = vistors[i].id;
                    lastvisiting = vistors[i].person_visiting;

                    console.log(vistors[i]);


                    obj = {
                        id: vistors[i].id,
                        name: vistors[i].first_name + " " + vistors[i].last_name,
                        company_representing: vistors[i].company_representing,
                        lastvisiting: vistors[i].person_visiting,
                        lastcheckintime: lastcheckintime
                    };

                    console.log(obj);

                    $scope.items.push(obj);

                }
            }        

                console.log( matchcount );    
            
            if (matchcount > 0  && searchVal.length >= 3) {


                $('#checkoutpicker').show();                
            }
            else
            {
                $('#checkoutpicker').hide();                         
            }
            
        }
        else {
            $('#checkoutpicker').hide();
        }
    };    
    
    // $scope.$broadcast('FORM_ERROR');
    $scope.checkout = function() {
        
        $scope.buttondisbled = "true";
        
        saveCheckout(checkoutid);

        for (var i=0;i < vistors.length;i++)
        {
            if ( vistors[i].id == checkoutid) {
                patient = vistors[i].person_visiting;
                person_visiting = vistors[i].person_visiting;

                if (!patient) {
                    patient =  vistors[i].company_representing;                    
                }

            }

        }

        

    };


    $scope.selectcheckout = function(visitorid) {

        if (debug) { console.log(visitorid); }
        for (var i=0;i < vistors.length;i++)
        {


            if (vistors[i].id == visitorid) {

                lastmatch = vistors[i].first_name + " " + vistors[i].last_name;
                lastemail = vistors[i].email; 
                lastcheckintime = vistors[i].checkin_time.substring(11, 16);
                lastid = vistors[i].id;  
                lastvisiting =  vistors[i].person_visiting;

                if (!lastvisiting) {
                    lastvisiting =  vistors[i].company_representing;                    
                }


                if (debug) { 
                    console.log('======'); 
                    console.log(vistors[i]); 
                    console.log(vistors[i].person_visiting);    
                    console.log(lastvisiting);                                      
                    console.log('======'); 
                }

                $('#checkoutpicker').hide(); 
                $scope.selectMatch();

                i=9999999999999999999999999;

            }

        }

    };

    $scope.selectMatch = function() {

        checkoutid = lastid;
        patient = lastvisiting;
        $('#checkout_name').val(lastmatch + "(" + lastcheckintime + ")");
        $('#checkoutbutton').show();
        if (user.apptype == 'custr') {

            

            if (user.smiles != 'Y') {
                if (user.distanceflag == 'Y') {
                     $('#distbit').show(); 
                }
            }
            else
            {
                $('#ratingbit').show();
            }
           
        }
        else 
        {
            $('#ratingbit').show();
        }
        $('#didyoumean').hide();  
    };        

    
    $scope.checkfinish = function() {
        patient = lastvisiting;//$('#checkin_name').val();
        $('#spinner').hide();
        $interval.cancel(localpromise);         
        myNavigator.replacePage('thankscheckout.html');
    };        

    $scope.iconclick = function(ratingid) {

        rating = ratingid;

        $('.q1').addClass('notselected');
        $('#' + ratingid).removeClass('notselected');     
        $('#' + ratingid).addClass('bigger');  

        
    };

    $scope.distclick = function(distid) {

        distance = distid;


        $('.d1').addClass('notselected');
        $('#' + distid).removeClass('notselected');     
        $('#' + distid).addClass('biggertext');  

        if (user.postcode == 'Y') {
            $('#distbit').hide();
            $('#postcodediv').show();
            $('#checkout_postcode').focus();
        }

        
    };    

    $scope.changepostcode = function() {

        // force reg to upper case
        $('#checkout_postcode').val($('#checkout_postcode').val().toUpperCase());
        
        postcode = $('#checkout_postcode').val();
    };
 
});


