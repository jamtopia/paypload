// This is a JavaScript file

myApp.controller('handsController', function($scope,$http,$interval,$rootScope) {

    $scope.checkin_name = patient;
    $scope.message = user.message;
    $scope.company_name = user.terminal_name;
    $scope.fire_image = 'img/blank.png';
    
    
    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;    

    $scope.bgimage = "img/blank.png";   
    if ( user.background_image !== undefined ) {
        $scope.bgimage = baseurl + "/" + user.background_image; 
    }
       
    $scope.company_image = "img/blank.png";   
    if ( user.logo_path !== undefined ) {
        $scope.company_image = baseurl + "/" + user.logo_path;
    }
        
    var localpromise;
    
    $scope.redirectBack = function() {

        $('#spinner').hide();
        $interval.cancel(localpromise);

        if (checkin_categoryVistorRequest) { 
            if (user.reverse == 'Y') { 
                myNavigator.replacePage('checkin.html', { animation : 'none' });
            }
            else
            {
                
                saveCheckin();                
            }
        } else {


            patient = '';
            patient_id = -1;
            get_here =  '';
            checkin_reg = '';             
            saveCheckin();    
            
        }
    };
    
    $scope.init=function(){
        
        var delay=10;

        if (user.attachement_path.split('.').pop() == 'jpg') {
            $scope.fire_image = baseurl + "/" + user.attachement_path;
            $('#firescroll').show();
            $('#thanksText').hide();
            delay=40000;
        }

        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);



    };

    $scope.$on('$destroy', function(){
        $interval.cancel(localpromise);
    });        
    
});    