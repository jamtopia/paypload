

myApp.controller('registerController', function($scope,$http,$interval,$rootScope) {

	var reg_fullname = "";    
    var reg_email = "";    
    var reg_phone = "";    
    var reg_company = "";    
    var reg_password = "";  

    $scope.registerbtndisbled = "";
    $scope.formOK = false;

    $('#fullnameCross').hide();
    $('#fullnameTick').hide();
    $('#emailCross').hide();
    $('#emailTick').hide();
    $('#phonenoCross').hide();
    $('#phonenoTick').hide();
    $('#companyCross').hide();
    $('#companyTick').hide();
    $('#passwordCross').hide();
    $('#passwordTick').hide();

    $scope.init = function () {
        $('#register_fullname').focus();
    };
    
    $scope.showTermCondition=function() {
        //$interval(myNavigator.resetToPage('terms.html'), 3000, 1); 
    }
        $scope.watchEnter = function(keyEvent,step) {

        if (keyEvent.which === 13)
        {
            if ( step == 1) {
                $('#register_email').focus();
            }
            if ( step == 2) {
                    $('#register_phoneno').focus();
            }
            if ( step == 3) {
                $('#register_company').focus();
            }
            if ( step == 4) {
                $('#register_password').focus();
            }

        }    
    };

        $scope.registerMe = function() {
            validateRegistration(true);
            if($scope.formOK){

                $('#spinner').show();
                $scope.registerbtndisbled = true;

                $http({
                method  : 'POST',
                url     : baseurl + "/api/register",
                data    : $.param({full_name: $('#register_fullname').val()
                                    , user_email: $('#register_email').val()
                                    , phone_number: $('#register_phoneno').val()
                                    , company_name: $('#register_company').val()
                                    , new_password: $('#register_password').val()
                                    }),  // pass in data as strings
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
            }).success(function(data) {
                
                if (data.error) {
                    ons.notification.alert({
                        message: data.message
                    });
                    $('#spinner').hide();
                    $scope.registerbtndisbled = "";
                } else {
                    $('#spinner').hide();
                    $interval(myNavigator.resetToPage('trial.html'), 3000, 1);
                }
              }).error(function (data) {
                    /* display proper error message */

                if ( data == null ) {
                    ons.notification.alert({
                        message: 'Unable to contact server, please check the wifi'
                    });
                    $('#spinner').hide();
                    $scope.registerbtndisbled = "";                    
                } else  {
                    ons.notification.alert({
                        message: data.message
                    });
                    $('#spinner').hide();
                    $scope.registerbtndisbled = "";
                } 
                    
              });
            }
            else
            {
                //console.log("Form not ok");
            }
    };   
    function validateRegistration(jumptofirsterror)
    {
            reg_fullname = $('#register_fullname').val();
            reg_email = $('#register_email').val();
            reg_phone = $('#register_phoneno').val();
            reg_company = $('#register_company').val();
            reg_password = $('#register_password').val(); 

            $scope.formOK = true;
            if (jumptofirsterror == undefined) {
            jumptofirsterror = false;
        }

        if(reg_fullname=="" || reg_fullname.trim() =="" || !isValidText(reg_fullname)){
            $scope.formOK = false;
            $('#fullnameCross').show();
            $('#fullnameTick').hide();
        }
        else{
            $('#fullnameCross').hide();
            $('#fullnameTick').show();
        }

        if(reg_email=="" || reg_email.trim() =="" || !isValidEmail(reg_email)){
            if ( $scope.formOK ) { 
                if (jumptofirsterror) { 

                    $('#register_email').focus(); 
                    step=1; 
           
                }
            }
            $scope.formOK = false;
            $('#emailCross').show();
            $('#emailTick').hide();
        }
        else{
            $('#emailCross').hide();
            $('#emailTick').show();
        }
        if(reg_phone=="" || reg_phone.trim() =="" || reg_phone.length<10 || !isValidNumber(reg_phone)){
            if ( $scope.formOK ) { 
                if (jumptofirsterror) { 

                    $('#register_phoneno').focus(); 
                    step=2; 
           
                }
            }
            $scope.formOK = false;
            $('#phonenoCross').show();
            $('#phonenoTick').hide();
        }
        else{
            $('#phonenoCross').hide();
            $('#phonenoTick').show();
        }
        if(reg_company =="" || reg_company.trim() =="" || !isValidText(reg_company)){
            if ( $scope.formOK ) { 
                if (jumptofirsterror) { 

                    $('#register_company').focus(); 
                    step=3; 
           
                }
            }
            $scope.formOK = false;
             $('#companyCross').show();
            $('#companyTick').hide();
        }
        else{
             $('#companyCross').hide();
            $('#companyTick').show();
        }
        if(reg_password=="" || reg_password.length<6 || reg_password.trim() ==""){
            if ( $scope.formOK) { 
                if (jumptofirsterror) { 

                    $('#register_password').focus(); 
                    step=4; 
           
                }
            }
            $scope.formOK = false;
            $('#passwordCross').show();
            $('#passwordTick').hide();
        }
        else{
            $('#passwordCross').hide();
            $('#passwordTick').show();
        }
    };  
    function isValidEmail(email) {
        var regex = /^([\w-\.\+]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return regex.test(email);
};
function isValidNumber(phoneno){
    var regexNo = /^[0-9]+$/;
    return regexNo.test(phoneno);
};
function isValidText(strText){
    if(checkForSpecialChar(strText)){ return false; }
    else { return true; }
};


 function checkForSpecialChar(string){
var specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\,./~`-=";
 for(i = 0; i < specialChars.length;i++){
   if(string.indexOf(specialChars[i]) > -1){ return true; }
 }
 return false;
}

});