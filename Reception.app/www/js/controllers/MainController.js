// This is a JavaScript file

myApp.controller('mainController', function($scope,$http,$interval,$sce,$rootScope) {
    
    var mainpromise;
    var mainprocess;
    var localpromise;
    var firerestarton = false;

    $scope.company_name = user.terminal_name;

    $scope.videopath = baseurl + "/" + user.video_path;
    $scope.videopath2 = "-";
    
    if (user.scrollmessage === null ) {
        user.scrollmessage = "";
    }
    
    $scope.scrollmessage = $sce.trustAsHtml(user.scrollmessage.replace(/\n/g,"<br>"));

    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;    
    $scope.checkinbgcolor = user.checkin_bgcolor;    
    $scope.checkincolor = user.checkin_color;    
    $scope.checkoutbgcolor = user.checkout_bgcolor;    
    $scope.checkoutcolor = user.checkout_color;  

    $scope.onlinecolor = '#7ed321';

    $scope.bigTime = "";


    $scope.bgimage = "img/blank.png";   
    if ( user.background_image !== undefined ) {
        $scope.bgimage = baseurl + "/" + user.background_image; 
        $rootScope.rootbgImage = baseurl + "/" + user.background_image; 
    }
       
    $scope.company_image = "img/blank.png";   
    if ( user.logo_path !== undefined ) {
        $scope.company_image = baseurl + "/" + user.logo_path;
    }    

    $scope.footer_image = "img/blank.png";   


    if ( user.footer_path !== undefined ) {
        $scope.footer_image = baseurl + "/" + user.footer_path;
    }   

    // Set th backgrounds and videos
    if ( user.video_path === undefined || user.video_path == '') {
        $rootScope.rootbgVideoShow = false;
    }        
    else
    {
        $rootScope.rootbgVideoShow = true;
        $rootScope.rootbgVideo = baseurl + "/" + user.video_path;
    }    

    
    
    $scope.init=function(){

        // Set th backgrounds and videos
        if ( user.video_path === undefined || user.video_path === '') {
            $rootScope.rootbgVideoShow = false;
        }        
        else
        {
            $rootScope.rootbgVideoShow = true;
            $rootScope.rootbgVideo = baseurl + "/" + user.video_path;
        }   

        $('#spinner').hide();

        if (debug ) {
            console.log(user);
            console.log(user.access_token);
            console.log($scope.videopath);
        }

        $scope.incrementCounter();
        $scope.decideNextPage();
       

        timeinterval = 30000;
        processtimeinterval = 45000;
        if (debug) { 
            timeinterval = 1000; 
            processtimeinterval = 10000;
        }

        mainpromise = $interval($scope.incrementCounter, timeinterval); 
        mainprocess = $interval($scope.processCached, processtimeinterval);

        lastemailblank = true;
        
        $scope.refreshImages();
        
        if ( hasStaff ) {
            $('#staff-button').show();
        }
    
        if ( user.lostbadge == 'Y') {
            $('#staff-button').hide();
            $('#lost-button').show();
        }           


        // Prefill button logic to show the extra button.
        if (user.enable_prefill == 'Y') {
            if (user.enable_prefill_front_screen == 'Y') {
                $('#prefill_section').show();
            }
            else
            {
                $('#prefill_section').hide();
            }
        }
        else
        {
            $('#prefill_section').hide();
        }

        
    };
    

    $scope.fireEscape = function(material) {


        var mod = material ? 'material' : undefined;
        ons.notification.prompt({
            message: "Please enter your password to enter the Fire Escape Mode ....",
            modifier: mod,
            callback: function(password) {
                if ( savedpassword.toLowerCase() == password.toLowerCase()) {
                    myNavigator.pushPage('fire.html', { animation : 'none' });                    
                }
            }
        });
    };
    
    $scope.restartApp = function(material) {
        var mod = material ? 'material' : undefined;
        ons.notification.prompt({
            message: "Please enter your password to restart the application ....",
            modifier: mod,
            callback: function(password) {
                if ( savedpassword.toLowerCase() == password.toLowerCase()) {
                    if ( checkincache.length > 0 || checkoutcache.length) {
                        ons.notification.alert({
                            message: 'Please go online before you logoff you have offline data still to send.'
                        });
                    }
                    else
                    {
                        restartApp(false);
                    }
                }
            }
        });
        
    };


    $scope.showDebug = function(material) {
        var mod = material ? 'material' : undefined;
        ons.notification.prompt({
            message: "Please enter your password to show the debug info ....",
            modifier: mod,
            callback: function(password) {
                if ( savedpassword.toLowerCase() == password.toLowerCase()) {
                     myNavigator.pushPage('debug.html', { animation : 'none' });   
                }
            }
        });
        
    };


    $scope.refreshImages = function() {
        
        //
        // Get user updates
        //
 

        $http({
            method  : 'GET',
            url     : baseurl + "/api/loginupdate",
            headers: {
                "X-Auth-Token": user.access_token,   //If your header name has spaces or any other char not appropriate
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            dataType: 'json'
        }).success(function(data) { 
            
            user = data.results;

            
            if (debug ) { console.log(user);}  
            
            
            $scope.bgimage = "img/blank.png";   
            if ( user.background_image !== undefined ) {
                $scope.bgimage = baseurl + "/" + user.background_image; 
            }
               
            $scope.company_image = "img/blank.png";   
            if ( user.logo_path !== undefined ) {
                $scope.company_image = baseurl + "/" + user.logo_path;
                $('#company_image').show();
            }    
            else
            {
                $('#company_image').hide();
            }
        
            $scope.footer_image = "img/blank.png";   
            if ( user.footer_path !== undefined ) {
                $('#footer_image').show();
                $scope.footer_image = baseurl + "/" + user.footer_path;
            }
            else
            {
                $('#footer_image').hide();
            }
            
            $scope.videopath = baseurl + "/" + user.video_path;
            $('#videosource').attr('src',$scope.videopath);

            if ( user.video_path === undefined ) {
                $('#videoimg').attr('poster',$scope.bgimage); 
            }
        
            if (debug ) { console.log("User");console.log(user);}
        }).error(function (response) {
        });            
        

    };
    
    $scope.$on('$destroy', function(){      
        $interval.cancel(mainpromise);
        $interval.cancel(mainprocess);        
        $interval.cancel(localpromise);        
    });    

    $scope.processCached = function() {

        if (isOnline) {
            processCached($http,$scope);     
        }       

        if (isOnline) {
            processCachedOut($http,$scope);     
        }                 

        getSystemData($http);

        if (isOnline) {
            $scope.onlinecolor = '#7ed321';
        }
        else
        {
            $scope.onlinecolor = 'red';
        }


    };
    
    $scope.fireRestart = function() {
        restartApp(true);
    };


    $scope.incrementCounter = function() {
        
        if ( hasStaff ) {
            $('#staff-button').show();
        }
        if ( user.lostbadge == 'Y') {
            $('#staff-button').hide();
            $('#lost-button').show();
        }    
        
        $scope.decideNextPage();

        var currentTime = new Date ( );
        var currentHours = currentTime.getHours ( );
        var currentMinutes = currentTime.getMinutes ( );
        var currentSeconds = currentTime.getSeconds ( );
        
        if (currentHours == 7 && currentMinutes == 2) {
            if (!firerestarton) {
                console.log('fired restart in ' +  ((60 + parseInt(user.id)) * 1000) + ' milseconds');
                if (!hasPrinter) {
                    localpromise = $interval($scope.fireRestart, ((60 + parseInt(user.id)) * 1000) );             
                }
                firerestarton = true;
            }
        }


        // Pad the minutes and seconds with leading zeros, if required
        currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
        currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

        // Choose either "AM" or "PM" as appropriate
        var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

        // Convert the hours component to 12-hour format if needed
        currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

        // Convert an hours component of "0" to "12"
        currentHours = ( currentHours === 0 ) ? 12 : currentHours;

        // Compose the string for display
        var currentTimeString = currentHours + ":" + currentMinutes + " " + timeOfDay;
        
        
        $scope.bigTime = currentTimeString;

        var s = "2012-10-16T17:57:28.556094Z";
        $scope.v = {
            Dt: Date.now(),
            sDt: s,
            DDt: Date.parse(s)
        };          

    };
    
 


    $scope.decideNextPage = function() {

        $scope.nextpage = 'checkin.html';
        if (user.reverse == 'Y') { $scope.nextpage = 'details.html'; }
        if (showcategoriespage) {
            $scope.nextpage = 'categories.html';
        }   

    };


});