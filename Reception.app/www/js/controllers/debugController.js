// This is a JavaScript file

myApp.controller('debugController', function($scope,$http,$interval,$rootScope) {
    
    $scope.checkin_name = "";
    $scope.company_name = user.terminal_name;

    $scope.test = "";
    $scope.contButtonDisabled = true; 
    
    
    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;    

    $scope.items = [];    
    $scope.buttondisbled = "";    
    
    $scope.noneleft = false;

    $scope.init = function () {
 

        $rootScope.rootbgVideoShow = false;

        $scope.refreshBugList();
    
    };


    $scope.refreshBugList = function() {

            $scope.noneleft = true;

            $scope.items = [];    

            // Put rerrors and warnings in
            for (var i=0;i < logArray.errors.length;i++)
            {

                obj = {
                    description: "ERROR : " + JSON.stringify(logArray.errors[i]) ,color: "red"
                };
                $scope.items.push(obj);

            }
            // Put rerrors and warnings in
            for (var i=0;i < logArray.warns.length;i++)
            {

                obj = {
                    description: "WARNINGS : " + JSON.stringify(logArray.warns[i])
                    ,color: "orange"
                };
                $scope.items.push(obj);

            }
    };


    $scope.redirectBack = function() {

        $rootScope.rootbgVideoShow = true;
        $('#spinner').hide();
        myNavigator.popPage();

    };

    $scope.$on('$destroy', function(){
        //$interval.cancel(localpromise);
    });    
    
     
});


