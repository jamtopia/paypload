// This is a JavaScript file

myApp.controller('checkinController', function($scope,$http,$interval,$rootScope) {
    $scope.checkin_name = "";
    $scope.company_name = "";
    $scope.checkin_apptext = "Person";
    $scope.lastmatchedid = -1;
    $scope.lastmatchedname = "";
    $scope.actionTitle = "Who are you visiting today?";
    $scope.checkin_shaddow_text = "Please enter the name here...";
    $scope.showTravelOptional = true;
    $scope.dialog = null;
    var lastid = 0; 
    $scope.items = [];        
    
    var checkinid = -1;
    
   
    $scope.test = "";    
    $scope.checkin_id = -1;
    $scope.contButtonDisabled = true; 
    
    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;    
    $scope.company_name = user.terminal_name;
    
    
    var localpromise;
    var localpromisedialog;
    var localpromisecontinue;
  

    $scope.bgimage = "img/blank.png";   
    if ( user.background_image !== undefined ) {
        $scope.bgimage = baseurl + "/" + user.background_image; 
    }
       
    $scope.company_image = "img/blank.png";   
    if ( user.logo_path !== undefined ) {
        $scope.company_image = baseurl + "/" + user.logo_path;
    }
    
    $scope.init = function () {
        

        $rootScope.rootbgVideoShow = false;

           
        $interval($scope.focus, 100, 1);  
               
        
        if ( user.apptype == 'custr') {
             $scope.checkin_apptext = "company";
        }

        if (user.checkin_shaddow_text) {
            if (user.checkin_shaddow_text != '') {
                    $scope.checkin_shaddow_text = user.checkin_shaddow_text;
            }
        }

        if (user.hide_travel_checkin == 'Y')
        {
            $('#howdidyougethereshow').hide();
            user.validate_car_reg = 'N';
        }

        if (checkin_category != undefined)
        {
            if (checkin_category.hide_travel_checkin == 'Y')
            {
                $('#howdidyougethereshow').hide();
                user.validate_car_reg = 'N';             
            }
        }


        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);
        
        // Show the solitor button if needed
        if ( user.apptype == "custrsolicitors"  ) {
            $('#solicitorscheckinName').show();
        }

        // Change to optional text if vlidation is needed
        if (user.validate_car_reg == 'Y')
        {
            $scope.showTravelOptional = false;
            $('#checkin_reg').attr('placeholder','Enter reg no.') ;
        }        
        else
        {
            $scope.showTravelOptional = false;
            $('#checkin_reg').attr('placeholder','Enter reg no. (optional)') ;
        }
 
        
    };

    $scope.focus = function() {
        $('#checkin_name').focus();
    };    
    
    $scope.gethere = function(gethere) {
        $('#get_here').val(gethere);
        
        if (gethere == 'car') {
            $('#howdidyougethere').hide(); 
            $('#carregdiv').show();
            $('#checkin_reg').focus();
        }
        
        $('.iconitem').css('color','#848484');
        $('.iconitem').css('background-color','white');
        //$('.iconitem').css('text-shadow',$scope.textbgcolor);

        // Set the selected on
        $('#' + gethere + 'icon').css('background-color',user.checkin_bgcolor);
        $('.' + gethere + 'icon').css('background-color',user.checkin_bgcolor);
        $('#' + gethere + 'icon').css('color',user.checkin_color);
        $('.' + gethere + 'icon').css('color',user.checkin_color);
        //$('#' + gethere + 'icon').css('text-shadow',$scope.textcolor);

        $scope.travelCheck();    
        
    };
    
    $scope.changeTransport = function(){
        $('.iconitem').css('color','#848484');
        $('#get_here').val('');
        $('#howdidyougethere').show(); 
        $('#carregdiv').hide();    
    };


    $scope.showtopCheck = function() {
        $('#topcheckinName').show();
    };

    $scope.redirectBack = function() {


        $(document.activeElement).filter(':input:focus').blur();        
        if (user.reverse == 'Y') { 
            $('#spinner').show();     
            $('#checkin_name').blur();


            $scope.checkin_id = -1;
            var searchVal = $('#checkin_name').val();
            for (var i=0;i < company.length;i++)
            {

                if (company[i].rectype == 'normal' && company[i].rectype != 'staff') {

                    if (company[i].name.toLowerCase() == searchVal.toLowerCase() ) {
                        $scope.checkin_name = company[i].name;
                        $scope.checkin_id = company[i].id;
                    }
                }

            }            

            patient = $('#checkin_name').val();
            patient_id = $scope.checkin_id;
            get_here =  $('#get_here').val();
            checkin_reg = $('#checkin_reg').val();

            saveCheckin();         
        }
        else
        {
            $scope.redirectBackOnly();
        }
    };
    
    $scope.redirectBackOnly = function() {
            $rootScope.rootbgVideoShow = true;
            $('#spinner').hide();
            $interval.cancel(localpromise);            
            $('#checkin_name').blur();
            myNavigator.popPage();
    };

    $scope.$on('$destroy', function(){
        $interval.cancel(localpromise);
    });    
    
    
     $scope.selectcheckin = function(companyid) {

        // reset the company override message so it's blank and will change for each company
        company_override_message = '';

        for (var i=0;i < company.length;i++)
        {

            if (company[i].id == companyid) {

                lastmatch = company[i].name;
                lastid = company[i].id;   

                if (String(company[i].checkin_company_text).length > 5 ) {
                    company_override_message = company[i].checkin_company_text;
                }

                $('#checkinpicker').hide(); 
                $scope.selectMatch();

                i=9999999999999999999999999;

            }

        }

    };
    
    $scope.selectMatch = function() {
        $('#checkin_name').val($scope.checkin_name);
        // $('#didyoumean').hide();
        checkinid = lastid;
        $('#checkin_name').val(lastmatch);
        $('#checkinName').show();
        $('#didyoumean').hide();  

    };
        
    

    $scope.selectNoIdea = function() {
    
        // Final check and if the exact math is found set that as the compnay they are seeing
        
        $scope.checkin_id = -1;
        var searchVal = $('#checkin_name').val();
        for (var i=0;i < company.length;i++)
        {

            if (company[i].rectype == 'normal' && company[i].rectype != 'staff') {

                if (company[i].name.toLowerCase() == searchVal.toLowerCase() ) {
                    $scope.checkin_name = company[i].name;
                    $scope.checkin_id = company[i].id;
                     //matchcount++;
                }
            }

        }

        $scope.checkfinish();

    };       
    
    $scope.selectNoCompanies = function() {
    
        $scope.checkin_id = -1;
        var searchVal = $('#checkin_name').val();
        for (var i=0;i < company.length;i++)
        {

            if (company[i].rectype == 'normal' && company[i].rectype != 'staff') {

                if (company[i].name.toLowerCase() == searchVal.toLowerCase() ) {
                    $scope.checkin_name = company[i].name;
                    $scope.checkin_id = company[i].id;
                    // matchcount++;
                }
            }

        }

        $scope.checkfinish();

    };       

    $scope.selectPatient = function() {
    
        $('#checkin_name').val($scope.checkin_name);
        
        var searchVal = $('#checkin_name').val();
        for (var i=0;i < company.length;i++)
        {

            if (company[i].rectype == 'normal' && company[i].rectype != 'staff') {

                if (company[i].name.toLowerCase() == searchVal.toLowerCase() ) {
                    $scope.checkin_name = company[i].name;
                    $scope.checkin_id = company[i].id;
                    // matchcount++;
                }
            }

        }

        $scope.checkfinish();

    };       

    
    
    $scope.changecheckin = function() {

        // Reset the timer if a key is pressed
        $interval.cancel(localpromise);
        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);          

        // Reset the timer for the conitue button
        $interval.cancel(localpromisecontinue);
            var maxshow = 0;


        var showresult = false;
        var nocompanies = true;
        var exactmatch = false;
        var searchVal = $('#checkin_name').val();

        // force reg to upper case
        $('#checkin_reg').val($('#checkin_reg').val().toUpperCase());
        
       
        for (var i=0;i < company.length;i++)
        {
            if (company[i].rectype == 'normal') {        
                nocompanies = false;
            }
        }


        $scope.items = [];
        
        if (searchVal !== "" || searchVal.length > 3) {
            
            var matchcount = 0;
            var obj;
            var dupobj;        
            
            lastmatch = "";
            lastemail = "";
            lastcheckintime = "";
            checkingtime = "";
            lastid = 0;
            var duphit = false;

            for (var i=0;i < company.length;i++)
            {

                if (company[i].rectype == 'normal' && company[i].rectype != 'staff') {

                    if (company[i].name.toLowerCase() == searchVal.toLowerCase() ) {
                        $scope.checkin_name = company[i].name;
                        $scope.checkin_id = company[i].id;
                         matchcount++;
                    }
                    else{
                        $scope.contButtonDisabled = true;
                         
                        if (company[i].name.replace(/ {2,}/g, ' ').toLowerCase().indexOf(searchVal.toLowerCase()) === 0 )
                        {
                            $scope.checkin_name = company[i].name;
                            $scope.checkin_id = company[i].id;
                            //showresult = true;  
                            
                            matchcount++;
                        }
                    }

                var matchname = company[i].name;
                matchname = matchname.replace(/\s\s+/g, ' ');


                if (( matchname ).toLowerCase().indexOf(searchVal.toLowerCase()) >= 0 ) {                


                    // Removed becuase if the last compnay does match it hides the continue button
                    //$scope.contButtonDisabled = false;
                    maxshow = maxshow +1;
                    
                    matchcount++;
                                       
                    lastmatch = company[i].name;
                    lastid = company[i].id;
                    var extrcomp = '';

                    if ( company[i].company_name != '') {
                        extrcomp = ' - ' + company[i].company_name;
                    }

                    obj = {
                        id: company[i].id,
                        name: company[i].name + extrcomp
                    };

                    $scope.items.push(obj);

                }

               
                }
            }        
            
        }
        else {
            showresult = false;
            $scope.lastmatchedid = -1;
            $scope.lastmatchedname = ""; 
            $('#checkinpicker').hide();
        }

                
        if (matchcount > 0  && searchVal.length >= 3) {
            $('#checkinpicker').show();                
        }
        else
        {
            $('#checkinpicker').hide();                         
        }


        if ( !$scope.contButtonDisabled ) {
            $('#checkinName').hide();
            $('#didyoumean').hide();
        }else
        {

            if (nocompanies) {

                $('#didyoumean').hide();

                $('#checkinName').hide();
                if (searchVal == "") {    
                    $('#noCompaniesName').hide();
                    $('#continuenext').hide();
                }
                else
                {
                    $('#solicitorscheckinName').show();
                    $('#continuenext').show();
                }
                
            }
            else
            {

                $('#noCompaniesName').hide();
                if ( showresult ) {

                    $('#checkinName').show();
                    $('#solicitorscheckinName').hide();
                    $('#topcheckinName').hide();
                    $('#didyoumean').show();
                    $('#continuenext').hide();
                }
                else {
    
                    $('#continuenext').show();
                    $('#didyoumean').hide();
                    $('#checkinName').hide();
                    if ( user.apptype == "custrsolicitors" || user.apptype == "custr" || user.apptype == "custrnursing" ) {
                        $('#solicitorscheckinName').show();
                        localpromisecontinue = $interval($scope.showtopCheck, 5000, 1);                          
                        
                    }
                }
            }
        }        

        $scope.travelCheck();    

    };    

    $scope.travelCheck = function() {
 
            // final check if the travel type is needed
        

            if (user.validate_car_reg == 'Y')
            {

                if ( $('#get_here').val() == '' ) {
                    $('#checkinName').hide();
                    $('#solicitorscheckinName').hide();   
                    $('#topcheckinName').hide();                     
                                    
                    $('#travelCross').show();

                }
                else
                {

                    $('#travelCross').hide();   
                    $('#solicitorscheckinName').show();                                       

                    if ( $('#get_here').val() == 'car' ) {
                        if ( $('#checkin_reg').val() =='' ) {
                            $('#carregCross').show();
                            $('#carregTick').hide();   

                            $('#checkinName').hide();
                            $('#solicitorscheckinName').hide();    
                            $('#topcheckinName').hide();                            
                                                                                     
                        }
                        else
                        {
                            $('#carregCross').hide();
                            $('#carregTick').show();                        
                        }
                    }
                }
            }        
    };


    $scope.selectContinue = function() {
        $('#checkin_name').blur();    
        $('#topcheckinName').hide();                  
    };
    
    $scope.checkfinish = function() {

        patient = $('#checkin_name').val();
        patient_id = $scope.checkin_id;
        get_here =  $('#get_here').val();
        checkin_reg = $('#checkin_reg').val();


        // Check to see if they need to enter the travel type
        
        if (user.reverse == 'Y') { 
        

            checkin_badgeno = '';
            $interval.cancel(localpromise);


            if ( user.capturebadge == 'Y') {
                ons.createDialog('badge.html', {parentScope: $scope}).then(function(dialog) {
                    $scope.dialog = dialog;
                    dialog.show();
                    $interval($scope.focusdialog, 400, 1);  
                    localpromisedialog = $interval($scope.dialogClick, screenredirectdelay, 1);
                });        
            } 
            else
            {
                $('#spinner').show();    
                saveCheckin(); 
            }
            
        }
        else
        {                
            myNavigator.replacePage('details.html', { animation : 'none' });
        }        

    };        
    

    $scope.dialogClick = function() {
        checkin_badgeno=$('#checkin_badgeno').val();
        if (debug ) { console.log(checkin_badgeno); }

        // Only call if the 
        if ($scope.dialog) {
            $scope.dialog.hide();
        }

        $('#checkin_badgeno').blur();
        $interval.cancel(localpromisedialog);
        $('#spinner').show();            
        saveCheckin();          
    };

    $scope.focusdialog = function() {
        $('#checkin_badgeno').focus();
    };       

    $scope.$on('$destroy', function(){
        $interval.cancel(localpromise);
        $interval.cancel(localpromisedialog);
        $interval.cancel(localpromisecontinue);
        
    });    
    

});


