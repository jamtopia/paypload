// This is a JavaScript file

myApp.controller('dbsController', function($scope,$http,$interval,$rootScope) {
    $scope.dbs_checkin_name = "";
    $scope.company_name = user.terminal_name;

    var lastmatch = "";
    var lastemail = "";
    var lastcheckintime = "";
    var lastpersonvisiting="";
    var checkingtime = "";
    var lastid = 0;    
    
    var prefilledcheckinid = -1;
    var checkout;
    
    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;    
    
    $scope.buttondisbled = "";    

    $scope.items = [];

    var localpromise;
    


    $scope.init = function () {
        $rootScope.rootbgVideoShow = false;       
        $interval($scope.focus, 100, 1);  
        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);

    };



    $scope.focus = function() {
        $('#dbs_checkin_name').focus();
    };    


    $scope.redirectBack = function() {

        $rootScope.rootbgVideoShow = true;
        $('#spinner').hide();
        $interval.cancel(localpromise);        
        $(document.activeElement).filter(':input:focus').blur();        
        $('#dbs_checkin_name').blur(); 
        myNavigator.popPage();
    };


    $scope.$on('$destroy', function(){
        $interval.cancel(localpromise);
    });        

    
    $scope.changedbscheckin = function() {

        $interval.cancel(localpromise);
        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);            

        var searchVal = $('#dbs_checkin_name').val();       
        var maxshow = 0;
        
        $scope.items = [];
        
        if (searchVal !== "" ) {
            
            var matchcount = 0;
            var obj;
            
            lastmatch = "";
            lastemail = "";
            lastcheckintime = "";
            lastpersonvisiting = "";
            lastid = 0;


            if (company) {

                for (var i=0;i < company.length;i++)
                {
                    
                    if (company[i].rectype == 'dbs') {
      
                        var matchname = company[i].name.replace(/\s\s+/g, ' ');


                        if (( matchname  ).toLowerCase().indexOf(searchVal.toLowerCase()) === 0 ) {                

                            matchcount++;
                                           
                            lastmatch = company[i].name;
                            lastemail = company[i].representative_email; 
                            lastcheckintime = new Date();
                            lastpersonvisiting = '';
                            lastid = company[i].id;

                            var showcompany = '';
                            var lastcompany = '';

                            if ( company[i].company_name) {
                                if ( company[i].company_name != '') {
                                    showcompany = '- ' + company[i].company_name + '';                            
                                    lastcompany = company[i].company_name + '';                            
                                }
                            }

                            obj = {
                                lastmatch: company[i].name,
                                showcompany: showcompany,
                                lastcompany: lastcompany,
                                lastemail: company[i].representative_email,
                                lastcheckintime: new Date(),
                                lastpersonvisiting: '',
                                lastid: company[i].id,
                                dbsnumber: company[i].dbs_number,
                                dbsexpire: company[i].dbs_expire
                            };                     

                            $scope.items.push(obj);
                        }
                    }
                }        

            }
            
            if (matchcount > 0 &&searchVal.length >= 2) {

                $scope.dbs_checkin_name = lastmatch; //+ "(" + lastcheckintime + ")";
                $('#dbspicker').show();     
                $('#notmatched').hide();     

            }
            else
            {
                if ( searchVal.length >= 2 ) {
                    $('#dbspicker').hide();     
                    $('#notmatched').show();                     
                }
                else
                {
                    $('#dbspicker').hide();     
                    $('#notmatched').hide();                         
                }
            }
            
            
        }
        else {
            $('#dbspicker').hide();
            $('#notmatched').hide();     
        }

    };    
    
    $scope.dbscheckin = function() {

        $interval.cancel(localpromise);
        var nextpage = 'contractor.html';
        myNavigator.replacePage(nextpage, { animation : 'none' });

    };

    $scope.selectMatchByID = function(item) {

        checkin_firstname = lastmatch;
        checkin_reg = '';
        checkin_surname = '';
        checkin_email = item.lastemail;
        checkin_mobile = '';    
        checkin_company = item.lastcompany;    
        dbs_number = item.dbsnumber;
        dbs_expire = item.dbsexpire;        

        $('#dbs_checkin_name').val(lastmatch);
        $('#dbscheckinbutton').show();
        
        $('#dbspicker').hide();  
    }
        

    $scope.noMatchAction=function(){

        $interval.cancel(localpromise);
        var nextpage = 'categories.html';
        myNavigator.replacePage(nextpage, { animation : 'none' });

    };

});


