// This is a JavaScript file

myApp.controller('contractorController', function($scope,$http,$interval,$rootScope) {

    $scope.checkin_name = patient;
    $scope.message = user.message;
    $scope.company_name = user.terminal_name;
    $scope.sign_document = 'img/blank.png';
    $scope.sign_text = 'I confirm that I have been through the induction';    
    
    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;    

    $scope.contractorbuttons = false;
    $scope.signstyle = "width:80vw;height:50vw;margin-top:-10vw";

    $scope.bgimage = "img/blank.png";   
    if ( user.background_image !== undefined ) {
        $scope.bgimage = baseurl + "/" + user.background_image; 
    }
       
    $scope.company_image = "img/blank.png";   
    if ( user.logo_path !== undefined ) {
        $scope.company_image = baseurl + "/" + user.logo_path;
    }


    // dbs scope variables 
    $scope.dbsname = '';
    $scope.dbsnumber = '';
    $scope.dbsexpire = '';

        
    var localpromise;
    var localpromisedialog;

    var canvas;
    var signaturePad;


    
    $scope.init=function(){
        
        checkin_signature = '';
        checkin_signature_accepted = 'N';

        if (checkin_category != undefined)
        {
            if (checkin_category.sign_document.split('.').pop() == 'jpg') {
                $scope.sign_document = baseurl + "/" + checkin_category.sign_document;
                $('#firescroll').show();
                $('#thanksText').hide();
            }

            $scope.sign_text = checkin_category.sign_text;

        }

        // flick the switch if the dbs check is on
        if (user.dbs_check == 'Y') {
            $('#contractorbuttonsactions').hide();
            $('#dbsbuttons').show();
        }

        // flick the switch if the dbs check is on
        if (checkin_ndaEnabled == 'Y') {
            $('#contractorbuttonsactions').hide();
            $('#dbsbuttons').hide();
            $('#ndabuttons').show();
        }


        localpromise = $interval($scope.redirectBack, screenredirectdelay, 1);

        signwidthdiv = document.getElementById('mpCompanyName');

        canvas = document.getElementById('signcanvas');
      

        signaturePad = new SignaturePad(canvas,{
            onBegin: function(event) {
                // So they have started to sign so shrink down the document and show the buttons
                $scope.signstyle = "width:80vw;height:35vw;margin-top:-15vw";
                $scope.contractorbuttons = true;
                $scope.$apply();

            }
        });    


        resizeCanvas();


    };

    
    $scope.redirectYes = function() {

        checkin_signature = signaturePad.toDataURL();
        checkin_signature_accepted = 'Y';

        $('#spinner').hide();
        $interval.cancel(localpromise);
        $interval.cancel(localpromisedialog);


        if (checkin_categoryVistorRequest) { 

            myNavigator.replacePage('checkin.html', { animation : 'none' });  

        }
        else
        {
            patient = '';
            patient_id = -1;
            get_here =  '';
            checkin_reg = ''; 

            $interval.cancel(localpromise);
            $interval.cancel(localpromisedialog);

            $('#spinner').show();                    
            saveCheckin();                
        }

    };


    $scope.ndaAccept = function() {

        checkin_signature = signaturePad.toDataURL();
        checkin_signature_accepted = 'Y';

        $('#spinner').hide();
        $interval.cancel(localpromise);
        $interval.cancel(localpromisedialog);


        if (checkin_categoryVistorRequest) { 

            myNavigator.replacePage('checkin.html', { animation : 'none' });  

        }
        else
        {
            patient = '';
            patient_id = -1;
            get_here =  '';
            checkin_reg = ''; 

            $interval.cancel(localpromise);
            $interval.cancel(localpromisedialog);

            $('#spinner').show();                    
            saveCheckin();                
        } 

    };


    $scope.dbsAccept = function() {

        checkin_signature = signaturePad.toDataURL();
        checkin_signature_accepted = 'Y';

        var tempexpireDate = new Date(dbs_expire.replace(/ /g,'T'));

        if (tempexpireDate > (new Date()) )
        {

            $scope.dbsname = (checkin_firstname + ' ' + checkin_surname).trim() + '\'s';
            $scope.dbsnumber = dbs_number;
            $scope.dbsexpire = dbs_expire;


            ons.createDialog('dbsapproved.html', {parentScope: $scope}).then(function(dialog) {
                $scope.dialog = dialog;
                dialog.show();
                localpromisedialog = $interval($scope.dialogClickNo, screenredirectdelay, 1);
            });   
        }
        else
        {
            $scope.dbsname = (checkin_firstname + ' ' + checkin_surname).trim() + '\'s';
            $scope.dbsnumber = dbs_number;
            $scope.dbsexpire = dbs_expire;
            checkin_signature_accepted = 'I';

            ons.createDialog('dbswarning.html', {parentScope: $scope}).then(function(dialog) {
                $scope.dialog = dialog;
                dialog.show();
                localpromisedialog = $interval($scope.dialogClickNo, screenredirectdelay, 1);
            });  
        }
    }


    $scope.redirectNo = function() {

        checkin_signature = signaturePad.toDataURL();
        checkin_signature_accepted = 'N';

        ons.createDialog('contractorno.html', {parentScope: $scope}).then(function(dialog) {
            $scope.dialog = dialog;
            dialog.show();
            localpromisedialog = $interval($scope.dialogClickNo, screenredirectdelay, 1);
        });      


    };


    $scope.dialogClickNo = function() {

        if (checkin_categoryVistorRequest) { 

            myNavigator.replacePage('checkin.html', { animation : 'none' });  

        }
        else
        {
            patient = '';
            patient_id = -1;
            get_here =  '';
            checkin_reg = ''; 

            $interval.cancel(localpromise);
            $interval.cancel(localpromisedialog);

            $('#spinner').show();                    
            saveCheckin();                
        }       
    };    



    $scope.redirectBack = function() {

        $rootScope.rootbgVideoShow = true;
        $('#spinner').hide();
        $interval.cancel(localpromise);
        $interval.cancel(localpromisedialog);


        myNavigator.popPage();
    };


    function resizeCanvas() {

        var ratio =  Math.max(window.devicePixelRatio || 1, 1);
        canvas.width = 400 * ratio;
        canvas.height = 150 * ratio;
        canvas.getContext("2d").scale(ratio, ratio);
        signaturePad.clear(); // otherwise isEmpty() might return incorrect value
    }



    $scope.$on('$destroy', function(){
        $interval.cancel(localpromise);
        $interval.cancel(localpromisedialog);
        
    });        
    
});    