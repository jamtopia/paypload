// This is a JavaScript file

myApp.controller('thanksController', function($scope,$http,$interval,$rootScope) {

    $scope.checkin_name = patient;
    $scope.message = user.message;
    $scope.company_name = user.terminal_name;
    $scope.fire_image = 'img/blank.png';
    
    
    $scope.textcolor = user.text_color;    
    $scope.textbgcolor = user.text_bgcolor;    

    $scope.company_image = "img/blank.png";   
    if ( user.logo_path !== undefined ) {
        $scope.company_image = baseurl + "/" + user.logo_path;
    }        
    
    var localpromise;       

    
    $scope.init=function(){

        $('#spinner').hide();  

        console.log(company_override_message)      ;

        if (company_override_message != '') { 
            $scope.message = company_override_message;
        }


        localpromise = $interval($scope.redirectBack, thanksredirectdelay, 1);

    };

    $scope.$on('$destroy', function(){
        $interval.cancel(localpromise);
    });  
    
    
    
    $scope.redirectBack = function() {
        //Reset values to empty

         checkin_firstname ='';
         checkin_reg='';
         checkin_surname='';
         patient='';
         checkin_email='';
         patient_id='';
         checkin_mobile='';
         get_here='';
         optin='';
         checkin_company='';
         checkinTime='';
         checkin_categoryId='';
         checkin_categoryName='';
         checkin_badgeno='';
         person_visiting='';

        $rootScope.rootbgVideoShow = true;
        $interval.cancel(localpromise);        
        myNavigator.popPage();
        //myNavigator.resetToPage('main.html', { animation : 'none' });
    };    
    
});    